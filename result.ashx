﻿<%@ WebHandler Language="C#" Class="result" %>

using System;
using System.Web;
using Microsoft.Security.Application;

public class result : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) 
    {
        System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
        string vchPollId = Sanitizer.GetSafeHtmlFragment(context.Request["pid"]);
        string vchQuestionId = Sanitizer.GetSafeHtmlFragment(context.Request["qid"]);
        
        //Casting
        int viPollId = 0;
        int.TryParse(vchPollId, out viPollId);

        Result[] rs;

        if (string.IsNullOrEmpty(vchPollId) || string.IsNullOrEmpty(vchQuestionId))
        {
            rs = new Result[] { new Result(0, "Operation aborted: Invalid Request.")};
        }
        else
        {
            using (dsPollTableAdapters.taPoll otaPoll = new dsPollTableAdapters.taPoll())
            {
                dsPoll.dtPollDataTable odtPoll = otaPoll.fnPollById(viPollId);
                if (odtPoll.Count > 0)
                {
                    using (dsQuestionTableAdapters.taQuestion otaQuestion = new dsQuestionTableAdapters.taQuestion())
                    {
                        dsQuestion.dtQuestionDataTable odtQuestion = otaQuestion.fnQuestionById(vchQuestionId, viPollId);
                        if (odtQuestion.Count > 0)
                        {
                            using (dsAnswerTableAdapters.taAnswer otaAnswer = new dsAnswerTableAdapters.taAnswer())
                            {
                                dsAnswer.dtAnswerDataTable odtAnswer = otaAnswer.fnQuestionByPollIdQuestionId(viPollId, vchQuestionId);
                                if (odtAnswer.Count > 0)
                                {
                                    using (dsVotesTableAdapters.taVotesAllTotalVotes otaVotesAllTotalVotes = new dsVotesTableAdapters.taVotesAllTotalVotes())
                                    {
                                        dsVotes.dtVotesAllTotalVotesDataTable odtVotesAllTotalVotes = otaVotesAllTotalVotes.fnAllTotalVotes(viPollId, vchQuestionId, odtQuestion[0].vdtPollStart, odtQuestion[0].vdtPollEnd);
                                        rs = new Result[odtAnswer.Count];
                                        if (odtVotesAllTotalVotes.Count > 0)
                                        {
                                            double vdblTotal = 0.00f;
                                            for (int i = 0; i < odtVotesAllTotalVotes.Count; i++)
                                            {
                                                vdblTotal += odtVotesAllTotalVotes[i].viTotal;
                                            }

                                            for (int i = 0; i < odtAnswer.Count; i++)
                                            {
                                                int z=-1;
                                                for (int j = 0; j < odtVotesAllTotalVotes.Count; j++)
                                                {
                                                    if (odtVotesAllTotalVotes[j].viAnswerIdFk == odtAnswer[i].viId)
                                                    {
                                                        z = j;
                                                        break;
                                                    }
                                                }
                                                
                                                if(z == -1)
                                                {
                                                    rs[i] = new Result(odtAnswer[i].viId, odtAnswer[i].vchAnswertext, 0, 0.00f);
                                                }
                                                else
                                                {
                                                    rs[i] = new Result(odtAnswer[i].viId, odtAnswer[i].vchAnswertext, odtVotesAllTotalVotes[z].viTotal, Math.Round(((odtVotesAllTotalVotes[z].viTotal / vdblTotal) * 100.00f), 2));
                                                }                                                
                                            }
                                        }
                                        else //All 0
                                        {
                                            for (int i = 0; i < odtAnswer.Count; i++)
                                            {
                                                rs[i] = new Result(odtAnswer[i].viId, odtAnswer[i].vchAnswertext, 0, 0.00f);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    rs = new Result[] { new Result(0, "Operation aborted: Invalid Request, options not available.")};
                                }
                            }
                        }
                        else
                        {
                            rs = new Result[] {new Result(0, "Operation aborted: Invalid Request, question does not exist.")};
                        }
                    }
                }
                else
                {
                    rs = new Result[] {new Result(0, "Operation aborted: Invalid Request, poll does not exist.")};
                }
            }
        }
        
        string json = js.Serialize(rs);
        context.Response.Clear();
        context.Response.ContentType = "application/json; charset=utf-8";
        context.Response.Write(json);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

    public class Result
    {
        public int id { get; set; }
        public string ans_text { get; set; }
        public int total { get; set; }
        public double total_percentage { get; set; }

        public Result(int id, string ans_text)
        {
            this.id = id;
            this.ans_text = ans_text;
        }
        
        public Result(int id, string ans_text, int total, double total_percentage)
        {
            this.id = id;
            this.ans_text = ans_text;
            this.total = total;
            this.total_percentage = total_percentage;
        }
    }

}