﻿var userFbId = null;

// Load the SDK asynchronously
(function (d) {
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) { return; }
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    ref.parentNode.insertBefore(js, ref);
}(document));

function fnCheckLoginStatus()
{
    var docHeight = $(window).height();
    $("#loadingDiv").height(docHeight)
    .css({
        'opacity': 0.85,
        'position': 'absolute',
        'top': '0px',
        'left': '50px',
        'margin-left': '-50px',
        'background-color': 'white',
        'width': '100%',
        'z-index': 5000,
    });
    $('#loadingDiv').show();

    FB.getLoginStatus(function (response) {
        if (response.status === 'connected') {
            //Show Question
            FB.api('/me', function (response) {
                SetCookies(response);
            });
            $('#midbanner').show();
            $('#loadingDiv').hide()
        }
        else {
            $('#divFblogin').show();
            $('#loadingDiv').hide()
        }
    });
}

function fnFbLogin() {
    FB.login(function (response) {
        if (response.authResponse) {
            FB.getLoginStatus(function (response) {
                if (response.status === 'connected') {
                    FB.api('/me', function (response) {
                        SetCookies(response);
                        $('#midbanner').show();
                        $('#divFblogin').hide();
                    });
                } else if (response.status === 'not_authorized') {
                    fnFbLogin();
                } else {
                    fnFbLogin();
                }
            });
        } else {

        }
    }, { scope: 'email' });
}

function SetCookies(response) {
    $.cookie('fbname', response.name);
    $.cookie('fblink', response.link);
    $.cookie('fbid', response.id);
    $.cookie('fbemail', response.email);
}

function ClearCookies() {
    $.cookie('fbname', "");
    $.cookie('fblink', "");
    $.cookie('fbid', "");
    $.cookie('fbemail', "");
}

function isEmpty(obj) {
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop))
            return false;
    }
    return true;
}