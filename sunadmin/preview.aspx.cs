﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class sunadmin_preview : System.Web.UI.Page
{
    string cookiesName = System.Configuration.ConfigurationManager.AppSettings["cookieName"];
    protected void Page_Load(object sender, EventArgs e)
    {
        #region NoBrowserCache
        HttpContext.Current.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
        HttpContext.Current.Response.Cache.SetValidUntilExpires(false);
        HttpContext.Current.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.Cache.SetNoStore();
        #endregion

        //Check cookies
        if (!fnIsTokenValid(cookiesName))
        {
            Response.Redirect("./");
        }
        else
        {
            if (!IsPostBack) //First Loads
            {
                if (!string.IsNullOrEmpty(Request.QueryString["pid"]) && (!string.IsNullOrEmpty(Request.QueryString["qid"])))
                {
                    string vchQSPollId = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Request.QueryString["pid"]);
                    int viPollId = 0;
                    int.TryParse(vchQSPollId, out viPollId);
                    string vchQSQuestionId = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Request.QueryString["qid"]);

                    using (dsPollTableAdapters.taPoll otaPoll = new dsPollTableAdapters.taPoll())
                    {
                        dsPoll.dtPollDataTable odtPoll = otaPoll.fnPollById(viPollId);
                        if (odtPoll.Count > 0)
                        {
                            ltrTitle.Text = odtPoll[0].vchName + "'s Poll";
                            ltrBannerImg.Text = "<img alt=\"" + odtPoll[0].vchBannerFilename + "\" src=\"../upload/banner/" + odtPoll[0].vchBannerFilename + "\" />";
                            ltrRemarks.Text = odtPoll[0].vchRemarks;
                            if (!string.IsNullOrEmpty(odtPoll[0].vchFbPageId))
                            {
                                ltrFBLike.Text = "<div class=\"fb-like\" data-href=\"https://www.facebook.com/pages/-/" + odtPoll[0].vchFbPageId + "\" data-width=\"450\" data-show-faces=\"true\" data-share=\"false\" data-layout=\"standard\"></div>";
                            }

                            using (dsQuestionTableAdapters.taQuestion otaQuestion = new dsQuestionTableAdapters.taQuestion())
                            {
                                dsQuestion.dtQuestionDataTable odtQuestion = otaQuestion.fnQuestionById(vchQSQuestionId, viPollId);
                                if(odtQuestion.Count>0)
                                {
                                    ltrVoteTitle.Text = odtQuestion[0].vchTitle;
                                    ltrQuestion.Text = odtQuestion[0].vchQuestiontext;
                                    parent.Style.Add("background-color", "#" + odtQuestion[0].vchPollBgColor);
                                    parent.Style.Add("color", "#" + odtQuestion[0].vchPollFontColor);
                                    ltrViewResult.Text = "<a id=\"btnViewResult\" class=\"progress-button\" style=\"display: inherit; text-align: center; width: 160px; border: 2px solid #" + odtQuestion[0].vchPollFontColor + "; color: #" + odtQuestion[0].vchPollFontColor + ";\">View Result</a><a id=\"btnBacktovote\" class=\"progress-button\" style=\"display: none; text-align: center; width: 160px; border: 2px solid #" + odtQuestion[0].vchPollFontColor + "; color: #" + odtQuestion[0].vchPollFontColor + ";\"><< Back</a><hr id=\"afterbtnline\" style=\"width: 80%; align-self: center;\">";
                                    ltrSubmitBtn.Text = "<div id=\"divSubBtn\"><a id=\"btnVote\" style=\"cursor:pointer;\"><img alt=\"" + odtPoll[0].vchBtnFilename + "\" src=\"../upload/btn/" + odtPoll[0].vchBtnFilename + "\" /></a></div>";
                                    using (dsAnswerTableAdapters.taAnswer otaAnswer = new dsAnswerTableAdapters.taAnswer())
                                    {
                                        radbtnlstChoices.DataSource = otaAnswer.fnQuestionByPollIdQuestionId(viPollId, odtQuestion[0].vchQuestionId);
                                        radbtnlstChoices.DataBind();
                                    }

                                    hiddPollId.Value = odtPoll[0].viId.ToString();
                                    hiddQuesId.Value = odtQuestion[0].vchQuestionId;
                                }
                                else
                                {
                                    shareWrapperDiv.Style.Add("display", "none");
                                    ltrQuestion.Text = "No poll is available at the moment. Please come back again.";
                                }
                            }
                        }
                        else
                        {
                            Response.Redirect("./");
                        }
                    }
                }
                else
                {
                    Response.Redirect("./");
                }
            }
        }        
    }

    //Check whether cookies is created
    protected bool fnIsCookieCreated(string cookiesName)
    {
        HttpCookie cookie = Request.Cookies[cookiesName];
        if (cookie != null)
            return true;
        else
            return false;
    }

    //Check for valid authentication
    protected bool fnIsTokenValid(string cookieName)
    {
        if (fnIsCookieCreated(cookieName))
        {
            HttpCookie cookie = Request.Cookies[cookieName];
            using (dsAdminTableAdapters.taAdmin otaAdmin = new dsAdminTableAdapters.taAdmin())
            {
                dsAdmin.dtAdminDataTable odtAdmin = otaAdmin.fnGetUsernameByUsername(cookie["username"]);
                if (odtAdmin.Count > 0)
                {
                    if (cookie["token"] == string.Concat(odtAdmin[0].vchUsername, odtAdmin[0].vchPassword))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            return false;
        }
    }
}