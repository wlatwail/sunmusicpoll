﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="question.aspx.cs" Inherits="sunadmin_question" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>[Admin]Add/Edit Question</title>
    <meta name="robots" content="noindex, nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- For IE 9 and below. ICO should be 32x32 pixels in size -->
    <!--[if IE]><link rel="shortcut icon" href="../favicon.ico"><![endif]-->

    <!-- Touch Icons - iOS and Android 2.1+ 180x180 pixels in size. -->
    <link rel="apple-touch-icon-precomposed" href="../images/astro_.png">

    <!-- Firefox, Chrome, Safari, IE 11+ and Opera. 196x196 pixels in size. -->
    <link rel="icon" href="../favicon.ico">
    <link href='//fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link href="../css/question.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../css/DateTimePicker.css" />
    <link rel="stylesheet" href="../css/colpick.css" type="text/css" />
    <link href="../css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
    <!--[if lt IE 9]>
      <link rel="stylesheet" type="text/css" href="../css/DateTimePicker-ltie9.css" />
    <![endif]-->
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
</head>
<body id="bodyhtml" runat="server">
    <form id="frmMain" runat="server" class="cbp-mc-form">
        <div class="container">
            <header class="clearfix">
                <span>
                    <asp:Literal ID="ltrErrMsg" runat="server"></asp:Literal>
                </span>
                <h1>Create / Edit Question for [<asp:Literal ID="ltrPollName" runat="server"></asp:Literal>] Poll</h1>
                <nav>
                    <asp:Literal ID="ltrBack" runat="server"></asp:Literal>
                    <asp:LinkButton ID="lnkBtnLogout" class="cd-btn" runat="server" OnClick="lnkBtnLogout_Click">Logout</asp:LinkButton>
                </nav>
            </header>
            <div class="cbp-mc-column">
                <label for="txtPollName">Poll Name</label>
                <asp:TextBox ID="txtPollName" Enabled="false" runat="server"></asp:TextBox>
                <label for="txtFbPageId">FB Page ID</label>
                <asp:TextBox ID="txtFbPageId" Enabled="false" runat="server"></asp:TextBox>
                <label for="txtRemarks">Remarks</label>
                <asp:TextBox ID="txtRemarks" Enabled="false" runat="server" TextMode="MultiLine"></asp:TextBox>                
            </div>
            <div class="cbp-mc-column">                
                <label for="txtPollCreatedDatetime">Created on:</label>
                <asp:TextBox ID="txtPollCreatedDatetime" Enabled="false" runat="server"></asp:TextBox>
                <label for="imgBtn">Button Image:</label>
                <asp:Image ID="imgBtn" runat="server" />
                <label for="imgBanner">Banner Image:</label>
                <asp:Image ID="imgBanner" runat="server" />
            </div>
            <hr />
            <div class="cbp-mc-fullcolumn">
                <label for="txtTitle">Title</label>
                <asp:TextBox ID="txtTitle" CssClass="validate[required, maxSize[50]]" placeholder="(max char: 50)" runat="server" MaxLength="50"></asp:TextBox>
                <label for="txtShowPoll">Poll is Public / Private ? (P/S: Poll will not show if poll is private)</label>
                <asp:TextBox ID="txtShowPoll" Enabled="false" runat="server"></asp:TextBox>
                <label for="txtChoices">Period</label>
                <asp:TextBox ID="txtdtQuestionStart" data-field="date" placeholder="Start Date" CssClass="txtDt validate[required,past[#txtdtQuestionEnd]]" runat="server" MaxLength="20"></asp:TextBox>
                TILL
                <asp:TextBox ID="txtdtQuestionEnd" data-field="date" placeholder="End Date" CssClass="txtDt validate[required,future[#txtdtQuestionStart]]" runat="server" MaxLength="20"></asp:TextBox>
                <label for="txtQuestion">Question</label>
                <asp:TextBox ID="txtQuestion" runat="server" placeholder="(max char: 500; best: 95)" CssClass="validate[required, maxSize[500]]" MaxLength="500"></asp:TextBox>
                <label for="txtChoices">Choices (max char.: 300 for each)</label>
                <a class="btnClick" id="btnAddChoices">Add (+)</a>
                <div id="TextBoxContainer">
                    <div>
                        <input name="DynamicTxtbx" class="inputDefaultAns validate[required, maxSize[300]]" type="text" />
                    </div>
                    <div>
                        <input name="DynamicTxtbx" class="inputDefaultAns validate[required, maxSize[300]]" type="text" />
                    </div>
                </div>
                <label for="txtBgColor">Background Colour & Font Colour</label>
                <asp:TextBox ID="txtBgColor" CssClass="txtDt validate[required]" runat="server" placeholder="Background Colour" MaxLength="30"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:TextBox ID="txtFontColor" CssClass="txtDt validate[required]" runat="server" placeholder="Font Colour" MaxLength="30"></asp:TextBox>
                <label for="ddlCapping">Vote Capping?</label>
                <asp:DropDownList ID="ddlCapping" CssClass="validate[required]" runat="server">
                    <asp:ListItem Value="">---- SELECT ----</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                    <asp:ListItem Value="2">No</asp:ListItem>
                </asp:DropDownList>
                <div style="display: none;" id="divVoteCapping" runat="server">
                    <label for="ddlCapping">Number of votes / Day</label>
                    <asp:TextBox ID="txtVotelimit" runat="server" placeholder="Limit" MaxLength="5"></asp:TextBox>
                </div>
                <label for="txtFbOgDiscription">FB OG Description (CTA)</label>
                <asp:TextBox ID="txtFbOgDiscription" runat="server" placeholder="(max char: 300)" CssClass="validate[required, maxSize[300]]" TextMode="MultiLine"></asp:TextBox>
                <fieldset style="margin-top: 2em;">
                    <legend>Pop-up</legend>
                    <label for="txtPopMsg">Pop-up Message</label>
                    <asp:TextBox ID="txtPopMsg" CssClass="validate[required, maxSize[300]]" runat="server" placeholder="Message after voting (max char: 300)" MaxLength="330"></asp:TextBox>
                    <label>Background Colour & Font Colour</label>
                    <asp:TextBox ID="txtPopBgcolor" CssClass="txtDt validate[required]" runat="server" placeholder="Background Colour" MaxLength="30"></asp:TextBox>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="txtPopFontcolor" CssClass="txtDt validate[required]" runat="server" placeholder="Font Colour" MaxLength="30"></asp:TextBox>
                    <label for="btnPopPreview">Preview Pop-up</label>
                    <a class="btnClick" id="btnPopPreviewWithResult">Preview (With Result Button)</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a class="btnClick" id="btnPopPreviewNoResult" style="margin-top: inherit;">Preview (Without Result Button)</a>
                </fieldset>
                <fieldset style="margin-top: 2em;">
                    <legend>Result</legend>
                    <label for="ddlChartType">Result Chart Type</label>
                    <asp:DropDownList ID="ddlChartType" CssClass="validate[required]" runat="server">
                        <asp:ListItem Value="">---- SELECT ----</asp:ListItem>
                        <asp:ListItem Value="1">Pie Chart</asp:ListItem>
                        <asp:ListItem Value="2">Bar Chart</asp:ListItem>
                    </asp:DropDownList>
                    <label for="ddlShowResult">Show Result</label>
                    <asp:DropDownList ID="ddlShowResult" CssClass="validate[required]" runat="server">
                        <asp:ListItem Value="">---- SELECT ----</asp:ListItem>
                        <asp:ListItem Value="0">Do Not Show</asp:ListItem>
                        <asp:ListItem Value="1">After Vote</asp:ListItem>
                        <asp:ListItem Value="2">Date Time</asp:ListItem>
                    </asp:DropDownList>
                    <div style="display: none;" id="divResultDatetime" runat="server">
                        <label for="ddlShowResult">Date Time</label>
                        <asp:TextBox ID="txtResultDatetime" runat="server" placeholder="Date Time" MaxLength="30" data-field="datetime"></asp:TextBox>
                    </div>
                </fieldset>
                <div class="cbp-mc-submit-wrap">
                    <asp:Button ID="btnSubmit" runat="server" Text="Save" OnClick="btnSubmit_Click" CssClass="cbp-mc-submit" />
                </div>
            </div>
            <hr />
        </div>
        <asp:HiddenField ID="hiddPollId" runat="server" />
        <asp:HiddenField ID="hiddQuesId" runat="server" />
        <div style="display: none;">
            <asp:Button ID="btnRefresh" runat="server" Text="Refresh" OnClick="btnRefresh_Click" />
        </div>
    </form>
    <div id="dtBox"></div>
    <div class="cd-popup" role="alert">
        <div class="cd-popup-container">
            <p id="pop-para">&nbsp;</p>
            <ul class="cd-buttons" id="cd-buttons-ul">
            </ul>
            <a href="#" class="cd-popup-close img-replace">Close</a>
        </div>
        <!-- cd-popup-container -->
    </div>
    <div class="cd-popup2" role="alert">
        <div class="cd-popup-container" style="background: #fff;">
            <p style="color: #000;">
                <asp:Literal ID="ltrInsUpdMsg" runat="server"></asp:Literal>
            </p>
            <ul class="cd-buttons">
                <li style='width: 100%;'><a href="#" class="cd-buttons-close">OK</a></li>
            </ul>
            <a href="#" class="cd-popup-close img-replace">Close</a>
        </div>
        <!-- cd-popup-container -->
    </div>
    <div id="loadingDiv" runat="server" style="display: none;">
        <img src="../images/bold.gif" alt="Loading" />
    </div>
    <!-- cd-popup -->
    <!-- JavaScript at the bottom for fast page loading -->
    <script type="text/javascript" src="../js/DateTimePicker.js"></script>
    <script src="../js/colpick.js" type="text/javascript"></script>
    <!--[if lt IE 9]>
      <script type="text/javascript" src="../js/DateTimePicker-ltie9.js"></script>
    <![endif]-->
    <script src="../js/jquery.validationEngine-en.js" type="text/javascript"></script>
    <script src="../js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
        function fnGetDynamicTextBox(value) {
            return '<input name="DynamicTxtbx" class="inputAns validate[maxSize[300]]" type="text" value="' + value + '" />' + '<a class="btnClickRemove" onclick="fnDeleteTextbox(this)">Remove</a>'
        }

        function fnGetFixedTextBox(value) {
            return '<input name="DynamicTxtbx" class="inputDefaultAns validate[required, maxSize[300]]" type="text" value="' + value + '" />'
        }

        function fnAddTextbox() {
            var div = document.createElement('DIV');
            div.innerHTML = fnGetDynamicTextBox("");
            var allDiv = document.getElementById("TextBoxContainer").children;
            if (allDiv.length < 10) {
                document.getElementById("TextBoxContainer").appendChild(div);
            }
        }

        function fnDeleteTextbox(div) {
            document.getElementById("TextBoxContainer").removeChild(div.parentNode);
        }

        function fnRecreateDynamicTextbox() {
            var values = eval('<%=Values%>');
            if (values != null) {
                var html = "";

                for (var i = 0; i < values.length; i++) {
                    if (i < 2) {
                        html += "<div>" + fnGetFixedTextBox(values[i]) + "</div>";
                    }
                    else {
                        html += "<div>" + fnGetDynamicTextBox(values[i]) + "</div>";
                    }
                }
                document.getElementById("TextBoxContainer").innerHTML = html;
            }
        }
        window.onload = fnRecreateDynamicTextbox;

        $(document).ready(function () {
            //open popup
            $('#btnPopPreviewWithResult').on('click', function (event) {
                event.preventDefault();
                if (($("#txtPopMsg").validationEngine('validate') == true) && ($("#txtPopBgcolor").validationEngine('validate') == true) && ($("#txtPopFontcolor").validationEngine('validate') == true)) {
                    $('#cd-buttons-ul').html("<li style='width: 50%;'><a class='cd-buttons-close'>View Result</a></li><li style='width: 50%;'><a class='cd-buttons-close'>Close</a></li>");
                    $('#pop-para').text($('#<%=txtPopMsg.ClientID%>').val());
                    $('#pop-para').css("color", '#' + $('#<%=txtPopFontcolor.ClientID%>').val());
                    $('.cd-popup-container').css("background", '#' + $('#<%=txtPopBgcolor.ClientID%>').val());
                    $('.cd-popup').addClass('is-visible');
                }
            });

            $('#btnPopPreviewNoResult').on('click', function (event) {
                event.preventDefault();
                if (($("#txtPopMsg").validationEngine('validate') == true) && ($("#txtPopBgcolor").validationEngine('validate') == true) && ($("#txtPopFontcolor").validationEngine('validate') == true)) {
                    $('#cd-buttons-ul').html("<li style='width: 100%;'><a class='cd-buttons-close'>Close</a></li>");
                    $('#pop-para').text($('#<%=txtPopMsg.ClientID%>').val());
                    $('#pop-para').css("color", '#' + $('#<%=txtPopFontcolor.ClientID%>').val());
                    $('.cd-popup-container').css("background", '#' + $('#<%=txtPopBgcolor.ClientID%>').val());
                    $('.cd-popup').addClass('is-visible');
                }
            });

            //close popup
            $('.cd-popup').on('click', function (event) {
                if ($(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup')) {
                    event.preventDefault();
                    $(this).removeClass('is-visible');
                }
            });

            //close popup
            $('.cd-popup').on('click', function (event) {
                if ($(event.target).is('.cd-buttons-close') || $(event.target).is('.cd-popup')) {
                    event.preventDefault();
                    $(this).removeClass('is-visible');
                }
            });

            //close popup
            $('.cd-popup2').on('click', function (event) {
                if ($(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup')) {
                    event.preventDefault();
                    $(this).removeClass('is-visible');
                }
            });

            //close popup
            $('.cd-popup2').on('click', function (event) {
                if ($(event.target).is('.cd-buttons-close') || $(event.target).is('.cd-popup')) {
                    event.preventDefault();
                    $(this).removeClass('is-visible');
                    $("#<%=btnRefresh.ClientID%>").trigger("click");
                }
            });

            $("#btnAddChoices").click(function (event) {
                fnAddTextbox();
            });

            $('#<%=ddlCapping.ClientID%>').change(function () {
                if ($('#<%=ddlCapping.ClientID%>').val() == "1") {
                    $('#divVoteCapping').show();
                    $('#<%=txtVotelimit.ClientID%>').addClass("validate[required,custom[integer],min[1]]");
                }
                else {
                    $('#divVoteCapping').hide();
                    $('#<%=txtVotelimit.ClientID%>').removeClass("validate[required,custom[integer],min[1]]");
                }
            });

            $('#<%=ddlShowResult.ClientID%>').change(function () {
                if ($('#<%=ddlShowResult.ClientID%>').val() == "2") {
                    $('#divResultDatetime').show();
                    $('#<%=txtResultDatetime.ClientID%>').addClass("validate[required]");
                }
                else {
                    $('#divResultDatetime').hide();
                    $('#<%=txtResultDatetime.ClientID%>').addClass("validate[required]");
                }
            });

            $("#dtBox").DateTimePicker({ 'dateFormat': 'yyyy-MM-dd', 'dateTimeFormat': 'yyyy-MM-dd HH:mm:ss' });

            $('#txtBgColor').colpick({
                layout: 'hex',
                submit: 0,
                colorScheme: 'light',
                onChange: function (hsb, hex, rgb, el, bySetColor) {
                    $(el).css({ 'border-right': '80px solid #' + hex });
                    // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
                    if (!bySetColor) $(el).val(hex);
                }
            }).keyup(function () {
                $(this).colpickSetColor(this.value);
            });

            $('#txtFontColor').colpick({
                layout: 'hex',
                submit: 0,
                colorScheme: 'dark',
                onChange: function (hsb, hex, rgb, el, bySetColor) {
                    $(el).css('border-right', '80px solid #' + hex);
                    // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
                    if (!bySetColor) $(el).val(hex);
                }
            }).keyup(function () {
                $(this).colpickSetColor(this.value);
            });

            $('#txtPopBgcolor').colpick({
                layout: 'hex',
                submit: 0,
                colorScheme: 'light',
                onChange: function (hsb, hex, rgb, el, bySetColor) {
                    $(el).css('border-right', '80px solid #' + hex);
                    // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
                    if (!bySetColor) $(el).val(hex);
                }
            }).keyup(function () {
                $(this).colpickSetColor(this.value);
            });

            $('#txtPopFontcolor').colpick({
                layout: 'hex',
                submit: 0,
                colorScheme: 'dark',
                onChange: function (hsb, hex, rgb, el, bySetColor) {
                    $(el).css('border-right', '80px solid #' + hex);
                    // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
                    if (!bySetColor) $(el).val(hex);
                }
            }).keyup(function () {
                $(this).colpickSetColor(this.value);
            });

            $("#frmMain").validationEngine('attach', { promptPosition: "bottomLeft" });

            $('#<%=btnRefresh.ClientID%>').on('click', function (event) {
                $("#frmMain").validationEngine('detach');
            });

            $('#<%=btnSubmit.ClientID%>').on('click', function (event) {
                if (($("#frmMain").validationEngine('validate') === true)) {
                    var docHeight = $(window).height();
                    $("#loadingDiv").height(docHeight)
                    .css({
                        'opacity': 0.85,
                        'position': 'absolute',
                        'top': 0,
                        'left': 0,
                        'background-color': 'white',
                        'width': '100%',
                        'z-index': 5000
                    });
                    $('#loadingDiv').show();
                    $('body').addClass('stop-scrolling');                    
                }                
            });
        });
    </script>

</body>
</html>
