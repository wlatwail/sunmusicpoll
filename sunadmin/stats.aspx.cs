﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class sunadmin_user : System.Web.UI.Page
{
    string cookiesName = System.Configuration.ConfigurationManager.AppSettings["cookieName"];

    protected void Page_Load(object sender, EventArgs e)
    {
        #region NoBrowserCache
        HttpContext.Current.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
        HttpContext.Current.Response.Cache.SetValidUntilExpires(false);
        HttpContext.Current.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.Cache.SetNoStore();
        #endregion

        //Check cookies
        if (!fnIsTokenValid(cookiesName))
        {
            Response.Redirect("./");
        }
        else
        {
            if (!IsPostBack) //First Loads
            {
                if (!string.IsNullOrEmpty(Request.QueryString["pid"]) && (!string.IsNullOrEmpty(Request.QueryString["qid"])))
                {
                    string vchQsPollId = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Request.QueryString["pid"]);
                    int viPollId = 0;
                    int.TryParse(vchQsPollId, out viPollId);
                    string vchQsQuestionId = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Request.QueryString["qid"]);

                    using (dsPollTableAdapters.taPoll otaPoll = new dsPollTableAdapters.taPoll())
                    {
                        dsPoll.dtPollDataTable odtPoll = otaPoll.fnPollById(viPollId);
                        if (odtPoll.Count > 0)
                        {
                            ltrBack.Text = "<a href=\"question-management.aspx?poll=" + viPollId + "\" class=\"cd-btn\">< Back</a>";
                            using (dsQuestionTableAdapters.taQuestion otaQuestion = new dsQuestionTableAdapters.taQuestion())
                            {
                                dsQuestion.dtQuestionDataTable odtQuestion = otaQuestion.fnQuestionById(vchQsQuestionId, viPollId);
                                if (odtQuestion.Count > 0)
                                {
                                    hiddPollId.Value = viPollId.ToString();
                                    hiddQuestionId.Value = odtQuestion[0].vchQuestionId;

                                    ltrPollTitle.Text = "[Stats]: " + odtPoll[0].vchName + "<br />" + odtQuestion[0].vchTitle + ": " + odtQuestion[0].vchQuestiontext;
                                    using (dsVotesTableAdapters.taVotesStanding otaVotesStanding = new dsVotesTableAdapters.taVotesStanding())
                                    {
                                        lstvwVoteStanding.DataSource = otaVotesStanding.fnVoteStanding(odtQuestion[0].vdtPollStart.Date, odtQuestion[0].vdtPollEnd.Date, viPollId, vchQsQuestionId);
                                        lstvwVoteStanding.DataBind();
                                    }

                                    using (dsVotesTableAdapters.taVotesDaily otaVotesDaily = new dsVotesTableAdapters.taVotesDaily())
                                    {
                                        System.Data.DataTable dt = new System.Data.DataTable();
                                        dt.Clear();
                                        dt.Columns.Add("vdtDate", System.Type.GetType("System.DateTime"));
                                        dt.Columns.Add("viTotal", System.Type.GetType("System.Int32"));

                                        dsVotes.dtVotesDailyDataTable odtVotesDaily = otaVotesDaily.fnDailyTotalVotes(viPollId, vchQsQuestionId, odtQuestion[0].vdtPollStart, odtQuestion[0].vdtPollEnd);
                                        DateTime vdtStartDate = odtQuestion[0].vdtPollStart;
                                        DateTime vdtEndDate = odtQuestion[0].vdtPollEnd;
                                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                                        sb.Append("<script>");
                                        sb.Append("$(function () { $('#container').highcharts({ chart: {type: 'line'},title: {text: 'Received Votes'},yAxis: {title: null},credits: {enabled: false},plotOptions: {line: {dataLabels: {enabled: true},enableMouseTracking: true}},");
                                        sb.Append("xAxis: {categories: [");

                                        System.Text.StringBuilder sb2 = new System.Text.StringBuilder();
                                        System.Text.StringBuilder sbData = new System.Text.StringBuilder();
                                        if (odtVotesDaily.Count > 0)
                                        {
                                            foreach (DateTime day in EachDay(vdtStartDate.Date, vdtEndDate.Date))
                                            {
                                                sb2.Append("'"); sb2.Append(day.Date.ToString("MMM dd, yyyy")); sb2.Append("',");
                                                System.Data.DataRow _row = dt.NewRow();
                                                _row["vdtDate"] = day.Date;
                                                _row["viTotal"] = 0;
                                                for (int i = 0; i < odtVotesDaily.Count; i++)
                                                {
                                                    if (day.Date == odtVotesDaily[i].vdtVotedDate.Date)
                                                    {
                                                        _row["viTotal"] = odtVotesDaily[i].viTotal;
                                                        break;
                                                    }
                                                }
                                                sbData.Append(_row["viTotal"] + ",");
                                                dt.Rows.Add(_row);
                                            }
                                        }
                                        else //No vote, yet
                                        {
                                            foreach (DateTime day in EachDay(vdtStartDate.Date, vdtEndDate.Date))
                                            {
                                                sb2.Append("'"); sb2.Append(day.Date.ToString("MMM dd, yyyy")); sb2.Append("',");
                                                System.Data.DataRow _row = dt.NewRow();
                                                _row["vdtDate"] = day.Date;
                                                _row["viTotal"] = 0;
                                                sbData.Append(_row["viTotal"] + ",");
                                                dt.Rows.Add(_row);
                                            }
                                        }

                                        sb.Append(sb2.ToString());
                                        sb.Append("]},series: [{name: 'Number of Votes',data: [");
                                        sb.Append(sbData.ToString());
                                        sb.Append("]}]});});");
                                        sb.Append("</script>");
                                        ltrHighcharts.Text = sb.ToString();
                                        lstVwDailyVotes.DataSource = dt;
                                        lstVwDailyVotes.DataBind();
                                    }

                                    using (dsVotesTableAdapters.taAllVotersVotes otaAllVotersVotes = new dsVotesTableAdapters.taAllVotersVotes())
                                    {
                                        dsVotes.dtAllVotersVotesDataTable odtAllVotersVotes = otaAllVotersVotes.fnVotersVotes(viPollId, vchQsQuestionId, odtQuestion[0].vdtPollStart, odtQuestion[0].vdtPollEnd);
                                        int total = 0;
                                        for (int i = 0; i < odtAllVotersVotes.Count; i++)
                                        {
                                            total += odtAllVotersVotes[i].viTotal;
                                        }
                                        ltrTotalVotesReceived.Text = total.ToString();
                                        lstVwUniqueVotes.DataSource = odtAllVotersVotes;
                                        lstVwUniqueVotes.DataBind();
                                    }
                                }
                                else //Question not found
                                {
                                    Response.Redirect("admin_.aspx");
                                }
                            }
                        }
                        else //Poll Not found
                        {
                            Response.Redirect("admin_.aspx");
                        }
                    }
                }
                else
                {
                    Response.Redirect("admin_.aspx");
                }
            }
        }
    }

    //Check whether cookies is created
    protected bool fnIsCookieCreated(string cookiesName)
    {
        HttpCookie cookie = Request.Cookies[cookiesName];
        if (cookie != null)
            return true;
        else
            return false;
    }

    //Check for valid authentication
    protected bool fnIsTokenValid(string cookieName)
    {
        if (fnIsCookieCreated(cookieName))
        {
            HttpCookie cookie = Request.Cookies[cookieName];
            using (dsAdminTableAdapters.taAdmin otaAdmin = new dsAdminTableAdapters.taAdmin())
            {
                dsAdmin.dtAdminDataTable odtAdmin = otaAdmin.fnGetUsernameByUsername(cookie["username"]);
                if (odtAdmin.Count > 0)
                {
                    if (cookie["token"] == string.Concat(odtAdmin[0].vchUsername, odtAdmin[0].vchPassword))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            return false;
        }
    }

    protected void btnLogout_Click(object sender, EventArgs e)
    {
        if (fnIsTokenValid(cookiesName))
        {
            //Start destroying cookie
            HttpCookie cookie = Request.Cookies[cookiesName];
            DateTime dt = DateTime.Now.AddYears(-1);
            cookie.Expires = dt;
            Response.Cookies.Add(cookie);
            Response.Redirect("./");
        }
        else
        {
            Response.Redirect("./");
        }
    }

    public IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
    {
        for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
            yield return day;
    }
}