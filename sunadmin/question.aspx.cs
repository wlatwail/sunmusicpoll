﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using TransactionHelper;

public partial class sunadmin_question : System.Web.UI.Page
{
    protected string Values;
    string cookiesName = System.Configuration.ConfigurationManager.AppSettings["cookieName"];

    protected void Page_Load(object sender, EventArgs e)
    {
        #region NoBrowserCache
        HttpContext.Current.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
        HttpContext.Current.Response.Cache.SetValidUntilExpires(false);
        HttpContext.Current.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.Cache.SetNoStore();
        #endregion
        
        //Check cookies
        if (!fnIsTokenValid(cookiesName))
        {
            Response.Redirect("./");
        }
        else
        {
            if (!IsPostBack) //First Loads
            {
                if (!string.IsNullOrEmpty(Request.QueryString["poll"]))
                {
                    string vchQSPollId = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Request.QueryString["poll"]);
                    int viPollId = 0;
                    int.TryParse(vchQSPollId, out viPollId);
                    using (dsPollTableAdapters.taPoll otaPoll = new dsPollTableAdapters.taPoll())
                    {
                        dsPoll.dtPollDataTable odtPoll = otaPoll.fnPollById(viPollId);
                        if (odtPoll.Count > 0)
                        {
                            ltrBack.Text = "<a href=\"question-management.aspx?poll=" + odtPoll[0].viId.ToString() +"\" class=\"cd-btn\">< Back</a>";
                            hiddPollId.Value = odtPoll[0].viId.ToString();
                            txtPollName.Text = odtPoll[0].vchName;
                            ltrPollName.Text = odtPoll[0].vchName;
                            txtFbPageId.Text = odtPoll[0].vchFbPageId;
                            txtRemarks.Text = odtPoll[0].vchRemarks;
                            txtShowPoll.Text = odtPoll[0].vblShowPoll ? "Public" : "Private";
                            txtPollCreatedDatetime.Text = odtPoll[0].vdtCreateddatetime.ToString("yyyy-MM-dd h:mm:ss tt");
                            imgBanner.ImageUrl = "../upload/banner/" + odtPoll[0].vchBannerFilename;
                            imgBtn.ImageUrl = "../upload/btn/" + odtPoll[0].vchBtnFilename;

                            if (!string.IsNullOrEmpty(Request.QueryString["question"]))
                            {
                                string vchQSQuestionId = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Request.QueryString["question"]);

                                using (dsQuestionTableAdapters.taQuestion otaQuestion = new dsQuestionTableAdapters.taQuestion())
                                {
                                    dsQuestion.dtQuestionDataTable odtQuestion = otaQuestion.fnQuestionById(vchQSQuestionId, viPollId);
                                    if (odtQuestion.Count > 0)
                                    {
                                        hiddQuesId.Value = odtQuestion[0].vchQuestionId;
                                        txtTitle.Text = odtQuestion[0].vchTitle;
                                        txtdtQuestionStart.Text = odtQuestion[0].vdtPollStart.ToString("yyyy-MM-dd");
                                        txtdtQuestionEnd.Text = odtQuestion[0].vdtPollEnd.ToString("yyyy-MM-dd");
                                        txtQuestion.Text = HttpUtility.HtmlDecode(odtQuestion[0].vchQuestiontext);

                                        using (dsAnswerTableAdapters.taAnswer otaAnswer = new dsAnswerTableAdapters.taAnswer())
                                        {
                                            dsAnswer.dtAnswerDataTable odtAnswer = otaAnswer.fnQuestionByPollIdQuestionId(viPollId, vchQSQuestionId);
                                            if (odtAnswer.Count > 0)
                                            {                                                
                                                List<string> listTxtValue = new List<string>();
                                                for (int i = 0; i < odtAnswer.Count; i++)
                                                {
                                                    listTxtValue.Add(odtAnswer[i].vchAnswertext);
                                                }
                                                string[] vchtxtbxValues = listTxtValue.ToArray();
                                                JavaScriptSerializer serializer = new JavaScriptSerializer();
                                                this.Values = serializer.Serialize(vchtxtbxValues);
                                            }
                                        }

                                        txtBgColor.Text = odtQuestion[0].vchPollBgColor;
                                        txtFontColor.Text = odtQuestion[0].vchPollFontColor;
                                        txtFbOgDiscription.Text = odtQuestion[0].vchFbCtaOgDescription;
                                        ddlCapping.SelectedValue = odtQuestion[0].vblIsVoteCapped ? "1" : "2";
                                        if (odtQuestion[0].vblIsVoteCapped)
                                        {
                                            txtVotelimit.Text = odtQuestion[0].viVoteLimit.ToString();
                                            divVoteCapping.Style.Remove("display");
                                            txtVotelimit.CssClass = "validate[required,custom[integer],min[1]]";
                                        }
                                        txtPopMsg.Text = odtQuestion[0].vchPopMsg;
                                        txtPopBgcolor.Text = odtQuestion[0].vchPopBgColor;
                                        txtPopFontcolor.Text = odtQuestion[0].vchPopFontColor;
                                        ddlChartType.SelectedValue = odtQuestion[0].viResultType.ToString();
                                        ddlShowResult.SelectedValue = odtQuestion[0].viShowResult.ToString();
                                        if (odtQuestion[0].viShowResult == 2)
                                        {
                                            txtResultDatetime.Text = odtQuestion[0].vdtShowResultDT.ToString("yyyy-MM-dd hh:mm");
                                            divResultDatetime.Style.Remove("display");
                                            txtResultDatetime.CssClass = "validate[required]";
                                        }
                                    }
                                    else
                                    {
                                        Response.Redirect("question-management.aspx?poll="+odtPoll[0].viId.ToString());
                                    }
                                }
                            }
                        }
                        else
                        {
                            Response.Redirect("admin_.aspx");
                        }
                    }
                }
                else
                {
                    Response.Redirect("admin_.aspx");
                }
            }
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string vchPollId = string.Empty;
        if (!string.IsNullOrEmpty(hiddPollId.Value))
            vchPollId = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(hiddPollId.Value);
        int viPollId = 0;
        int.TryParse(vchPollId, out viPollId);

        string vchQuestionId = string.Empty;
        if (!string.IsNullOrEmpty(hiddQuesId.Value))
            vchQuestionId = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(hiddQuesId.Value);

        if (!string.IsNullOrEmpty(vchPollId))
        {
            using (dsPollTableAdapters.taPoll otaPoll = new dsPollTableAdapters.taPoll())
            {
                dsPoll.dtPollDataTable odtPoll = otaPoll.fnPollById(viPollId);
                if (odtPoll.Count > 0)
                {
                    string[] vchtxtbxValues = Request.Form.GetValues("DynamicTxtbx");

                    #region Validation

                    if (string.IsNullOrEmpty(txtTitle.Text) || string.IsNullOrEmpty(txtdtQuestionStart.Text) || string.IsNullOrEmpty(txtdtQuestionEnd.Text) || string.IsNullOrEmpty(txtQuestion.Text) || string.IsNullOrEmpty(txtBgColor.Text) || string.IsNullOrEmpty(txtFontColor.Text) || ddlCapping.SelectedIndex == 0 || string.IsNullOrEmpty(txtPopMsg.Text) || string.IsNullOrEmpty(txtPopBgcolor.Text) || string.IsNullOrEmpty(txtPopFontcolor.Text) || ddlChartType.SelectedIndex == 0 || ddlShowResult.SelectedIndex == 0)
                    {
                        ltrErrMsg.Text = "*Submitted form is invalid, please complete all the required fields.";
                        loadingDiv.Style.Add("display", "none"); bodyhtml.Attributes.Remove("class");
                        //Response.Write(txtTitle.Text + " " + txtdtQuestionStart.Text + " " + txtdtQuestionEnd.Text + " " + txtQuestion.Text + " " + txtBgColor.Text + " " + txtFontColor.Text + " " + ddlCapping.SelectedIndex + " " + txtPopMsg.Text + " " + txtPopBgcolor.Text + " " + txtPopFontcolor.Text + " " + ddlChartType.SelectedIndex +" "+ ddlShowResult.SelectedIndex);
                        return;
                    }

                    #endregion

                    string vchTitle = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtTitle.Text);
                    DateTime vdtStart = Convert.ToDateTime(txtdtQuestionStart.Text);
                    DateTime vdtEnd = Convert.ToDateTime(txtdtQuestionEnd.Text);
                    string vchQuestion = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtQuestion.Text);
                    string vchPollBgColor = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtBgColor.Text);
                    string vchPollFontColor = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtFontColor.Text);
                    string vchFbOgDescription = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtFbOgDiscription.Text);
                    string vchIsCapped = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(ddlCapping.SelectedValue);
                    bool vblIsCapped = false;
                    int viVoteLimit = 0;
                    if (vchIsCapped == "1")
                    {
                        vblIsCapped = true;
                        string vchLimit = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtVotelimit.Text);
                        int.TryParse(vchLimit, out viVoteLimit);
                    }
                    string vchPopMsg = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtPopMsg.Text);
                    string vchPopBgColor = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtPopBgcolor.Text);
                    string vchPopFontColor = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtPopFontcolor.Text);
                    string vchChartType = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(ddlChartType.SelectedValue);
                    int viChartType = 0;
                    int.TryParse(vchChartType, out viChartType);
                    string vchShowResult = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(ddlShowResult.SelectedValue);
                    int viShowResult = 0;
                    int.TryParse(vchShowResult, out viShowResult);
                    DateTime vdtResultDT = DateTime.Now;
                    if (vchShowResult == "2")
                    {
                        string vchResultDT = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtResultDatetime.Text);
                        vdtResultDT = Convert.ToDateTime(vchResultDT);
                    }
                    string vchGeneratedQuestionId = System.Guid.NewGuid().ToString().Replace("-", "").Substring(0, 5) + DateTime.Now.ToString("yyyyMMddHHmmss");

                    SqlTransaction transaction = null;

                    try
                    {
                        if (!string.IsNullOrEmpty(vchQuestionId)) //Update
                        {
                            using (dsQuestionTableAdapters.taQuestion otaQuestion = new dsQuestionTableAdapters.taQuestion())
                            {
                                dsQuestion.dtQuestionDataTable odtQuestion = otaQuestion.fnQuestionById(vchQuestionId, viPollId);
                                if (odtQuestion.Count > 0)
                                {
                                    transaction = TableAdapterHelper.BeginTransaction(otaQuestion);
                                    otaQuestion.fnUpdQuestion(vchQuestion, vdtStart, vdtEnd, vblIsCapped, viVoteLimit, viChartType, vchTitle, vchPollBgColor, vchPollFontColor, vchPopMsg, vchPopBgColor, vchPopFontColor, vdtResultDT, viShowResult, vchFbOgDescription,vchQuestionId, viPollId);

                                    using (dsAnswerTableAdapters.taAnswer otaAnswer = new dsAnswerTableAdapters.taAnswer())
                                    {
                                        dsAnswer.dtAnswerDataTable odtAnswer = otaAnswer.fnQuestionByPollIdQuestionId(viPollId,vchQuestionId);
                                        if(odtAnswer.Count>0)
                                        {
                                            //Check if add or remove
                                            if (vchtxtbxValues.Length == odtAnswer.Count)
                                            {
                                                TableAdapterHelper.SetTransaction(otaAnswer, transaction);
                                                for (int i = 0; i < odtAnswer.Count; i++)
                                                {
                                                    otaAnswer.fnUpdAns(vchtxtbxValues[i], odtAnswer[i].viId);
                                                }
                                            }
                                            else if (vchtxtbxValues.Length > odtAnswer.Count)
                                            {
                                                int i = 0;
                                                TableAdapterHelper.SetTransaction(otaAnswer, transaction);
                                                while(i < odtAnswer.Count)
                                                {
                                                    otaAnswer.fnUpdAns(vchtxtbxValues[i], odtAnswer[i].viId);
                                                    i++;
                                                }

                                                while (i < vchtxtbxValues.Length)
                                                {
                                                    otaAnswer.fnInsAnswer(vchtxtbxValues[i], viPollId, vchQuestionId);
                                                    i++;
                                                }
                                            }
                                            else if (vchtxtbxValues.Length < odtAnswer.Count)
                                            {
                                                int i = 0;
                                                TableAdapterHelper.SetTransaction(otaAnswer, transaction);
                                                while (i < vchtxtbxValues.Length)
                                                {
                                                    otaAnswer.fnUpdAns(vchtxtbxValues[i], odtAnswer[i].viId);
                                                    i++;
                                                }

                                                while (i < odtAnswer.Count)
                                                {
                                                    otaAnswer.fnDltAns(odtAnswer[i].viId);
                                                    i++;
                                                }
                                            }                                           
                                        }
                                    }
                                    transaction.Commit();
                                    ltrInsUpdMsg.Text = "Question is updated.";
                                    loadingDiv.Style.Add("display", "none"); bodyhtml.Attributes.Remove("class");
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "click", "<script>jQuery(document).ready(function($){  $('.cd-popup2').addClass('is-visible'); });</script>", false);
                                }
                                else //Record does not exist
                                {
                                    Response.Redirect("question-management.aspx?poll="+odtPoll[0].viId.ToString());
                                }
                            }
                        }
                        else //Insert
                        {
                            //[Question] table
                            using (dsQuestionTableAdapters.taQuestion otaQuestion = new dsQuestionTableAdapters.taQuestion())
                            {
                                transaction = TableAdapterHelper.BeginTransaction(otaQuestion);
                                otaQuestion.fnInsQuestion(vchQuestion, viPollId, DateTime.Now, vdtStart, vdtEnd, vblIsCapped, viVoteLimit, viChartType, vchTitle, vchPollBgColor, vchPollFontColor, vchPopMsg, vchPopBgColor, vchPopFontColor, vdtResultDT, viShowResult, vchGeneratedQuestionId, vchFbOgDescription);
                            }

                            //[Answer] table
                            using (dsAnswerTableAdapters.taAnswer otaAnswer = new dsAnswerTableAdapters.taAnswer())
                            {
                                TableAdapterHelper.SetTransaction(otaAnswer, transaction);
                                foreach (string txtbxValue in vchtxtbxValues)
                                {
                                    otaAnswer.fnInsAnswer(txtbxValue, viPollId, vchGeneratedQuestionId);
                                }
                            }

                            transaction.Commit();
                            ltrInsUpdMsg.Text = "New Question created.";
                            loadingDiv.Style.Add("display", "none"); bodyhtml.Attributes.Remove("class");
                            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "click", "<script>jQuery(document).ready(function($){  $('.cd-popup2').addClass('is-visible'); });</script>", false);
                        }                        
                    }
                    catch
                    {
                        transaction.Rollback();
                        loadingDiv.Style.Add("display", "none"); bodyhtml.Attributes.Remove("class");
                        ltrErrMsg.Text = "Error: SQL Transaction Rolled Back!";
                    }
                    finally
                    {
                        transaction.Dispose();
                    }                    
                }
                else
                {
                    Response.Redirect("admin_.aspx");
                }
            }
        }
        else
        {
            Response.Redirect("admin_.aspx");
        }
    }

    //Check whether cookies is created
    protected bool fnIsCookieCreated(string cookiesName)
    {
        HttpCookie cookie = Request.Cookies[cookiesName];
        if (cookie != null)
            return true;
        else
            return false;
    }

    //Check for valid authentication
    protected bool fnIsTokenValid(string cookieName)
    {
        if (fnIsCookieCreated(cookieName))
        {
            HttpCookie cookie = Request.Cookies[cookieName];
            using (dsAdminTableAdapters.taAdmin otaAdmin = new dsAdminTableAdapters.taAdmin())
            {
                dsAdmin.dtAdminDataTable odtAdmin = otaAdmin.fnGetUsernameByUsername(cookie["username"]);
                if (odtAdmin.Count > 0)
                {
                    if (cookie["token"] == string.Concat(odtAdmin[0].vchUsername, odtAdmin[0].vchPassword))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            return false;
        }
    }

    protected void lnkBtnLogout_Click(object sender, EventArgs e)
    {
        if (fnIsTokenValid(cookiesName))
        {
            //Start destroying cookie
            HttpCookie cookie = Request.Cookies[cookiesName];
            DateTime dt = DateTime.Now.AddYears(-1);
            cookie.Expires = dt;
            Response.Cookies.Add(cookie);
            Response.Redirect("./");
        }
        else
        {
            Response.Redirect("./");
        }
    }

    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        string vchPollId = string.Empty;
        if (!string.IsNullOrEmpty(hiddPollId.Value))
            vchPollId = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(hiddPollId.Value);
        string vchQuestionId = string.Empty;
        if (!string.IsNullOrEmpty(hiddQuesId.Value))
            vchQuestionId = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(hiddQuesId.Value);
        if (!string.IsNullOrEmpty(vchPollId) && (!string.IsNullOrEmpty(vchQuestionId)))
        {
            Response.Redirect(Request.RawUrl);
        }
        else if (!string.IsNullOrEmpty(vchPollId))
        {
            Response.Redirect("question-management.aspx?poll=" + vchPollId);
        }
    }
}