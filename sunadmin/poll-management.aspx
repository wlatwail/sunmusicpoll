﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="poll-management.aspx.cs" Inherits="sunadmin_poll_management" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Poll Management</title>
    <meta name="robots" content="noindex, nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- For IE 9 and below. ICO should be 32x32 pixels in size -->
    <!--[if IE]><link rel="shortcut icon" href="../favicon.ico"><![endif]-->

    <!-- Touch Icons - iOS and Android 2.1+ 180x180 pixels in size. -->
    <link rel="apple-touch-icon-precomposed" href="../images/astro_.png">

    <!-- Firefox, Chrome, Safari, IE 11+ and Opera. 196x196 pixels in size. -->
    <link rel="icon" href="../favicon.ico">
    <link href="../css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
    <link href='//fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link href="../css/main.css" rel="stylesheet" type="text/css" />
    <link href="../css/question.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .tbl {
            width: 100%;
            box-sizing: border-box; /* causes item size to include border and padding */
            -moz-box-sizing: border-box; /*for browser compatibility*/
        }
    </style>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
</head>
<body>
    <form id="frmMain" runat="server" class="cbp-mc-form">
        <p>
            <a href="admin_.aspx" class="cd-btn">< Back</a>
            &nbsp;
            <asp:Literal ID="ltrQuestionLink" runat="server"></asp:Literal>
            <asp:LinkButton ID="lnkBtnLogout" class="cd-btn" runat="server" OnClick="lnkBtnLogout_Click">Logout</asp:LinkButton>
        </p>
        <asp:Literal ID="ltrErr" runat="server"></asp:Literal>
        <div>
            <fieldset>
                <legend>[Poll Management]: Add / Edit Poll</legend>
                <table class="tbl">
                    <tr>
                        <td>Poll Name</td>
                        <td>:</td>
                        <td>
                            <asp:TextBox CssClass="validate[required]" ID="txtPollname" placeholder="(max char: 500, best: 95)" runat="server" MaxLength="500" Style="width: 100%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Banner (W &#215; H: 1200px &#215; 630px)</td>
                        <td>:</td>
                        <td>
                            <asp:FileUpload ID="fluplBanner" runat="server" ToolTip="Select Banner" Style="width: 100%" />
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>jpg and png only</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>
                            <asp:Literal ID="ltrBannerImg" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>Public / Private</td>
                        <td>:</td>
                        <td>
                            <asp:DropDownList ID="ddlShowPoll" CssClass="validate[required]" runat="server" Style="width: 100%">
                                <asp:ListItem Value="">---- SELECT ----</asp:ListItem>
                                <asp:ListItem Value="1">Public</asp:ListItem>
                                <asp:ListItem Value="0">Private</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>Vote Button (W &#215; H: 106px &#215; 46px)</td>
                        <td>:</td>
                        <td>
                            <asp:FileUpload ID="fluplVoteBtn" runat="server" ToolTip="Select Banner" Style="width: 100%" />
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>jpg and png only</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>
                            <asp:Literal ID="ltrVoteBtn" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>FB page ID</td>
                        <td>:</td>
                        <td>
                            <asp:TextBox ID="txtFbPageID" MaxLength="50" runat="server" Style="width: 100%"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>FB App ID</td>
                        <td>:</td>
                        <td>
                            <asp:TextBox ID="txtFbAppId" MaxLength="50" runat="server" Style="width: 100%"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">Remarks</td>
                        <td style="vertical-align: top;">:</td>
                        <td>
                            <asp:TextBox ID="txtRemarks" runat="server" Height="70px" Style="width: 100%" placeholder="(max char: 7999; best: 300)" TextMode="MultiLine" MaxLength="7999"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:HiddenField ID="hiddPollId" runat="server" />
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>
                            <asp:Button ID="btnSave" CssClass="cbp-mc-submit" runat="server" Text="Save" OnClick="btnSave_Click" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div style="display: none">
            <asp:Label ID="lblPollName" runat="server" Text="POLL NAME"></asp:Label>
            <asp:Label ID="lblRemarks" runat="server" Text="EXTRA INFO ABOUT THE POLL"></asp:Label>
            <asp:Label ID="lblFbPageID" runat="server" Text="FB PAGE ID"></asp:Label>
            <asp:Button ID="btnRefresh" runat="server" Text="Refresh" OnClick="btnRefresh_Click" />
        </div>
        <div id="loadingDiv" runat="server" style="display: none;">
            <img src="../images/bold.gif" alt="Loading" />
        </div>
        <div class="cd-popup" role="alert">
            <div class="cd-popup-container">
                <p>
                    <asp:Literal ID="ltrMsg" runat="server"></asp:Literal>
                </p>
                <ul class="cd-buttons">
                    <li><a href="#" class="cd-buttons-close">OK</a></li>
                </ul>
                <a href="#" class="cd-popup-close img-replace">Close</a>
            </div>
            <!-- cd-popup-container -->
        </div>
        <!-- cd-popup -->
    </form>
    <!-- JavaScript at the bottom for fast page loading -->
    <script src="../js/jquery.validationEngine-en.js" type="text/javascript"></script>
    <script src="../js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            //close popup
            $('.cd-popup').on('click', function (event) {
                if ($(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup')) {
                    event.preventDefault();
                    $(this).removeClass('is-visible');
                }
            });

            //close popup
            $('.cd-popup').on('click', function (event) {
                if ($(event.target).is('.cd-buttons-close') || $(event.target).is('.cd-popup')) {
                    event.preventDefault();
                    $(this).removeClass('is-visible');
                    $("#<%=btnRefresh.ClientID%>").trigger("click");
                }
            });

            $('#<%=btnRefresh.ClientID%>').on('click', function (event) {
                $("#frmMain").validationEngine('detach');
            });
        });

        $(document).ready(function () {
            $("#frmMain").validationEngine('attach', { promptPosition: "topLeft" });

            $('#<%=btnSave.ClientID%>').on('click', function (event) {
                if (($("#frmMain").validationEngine('validate') == true)) {
                    var docHeight = $(document).height();
                    $("#loadingDiv").height(docHeight)
                    .css({
                        'opacity': 0.85,
                        'position': 'absolute',
                        'top': 0,
                        'left': 0,
                        'background-color': 'white',
                        'width': '100%',
                        'z-index': 5000
                    });

                    $('#loadingDiv').show();
                }
            });
        });
    </script>
</body>
</html>
