﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class sunadmin_poll_management : System.Web.UI.Page
{
    string cookiesName = System.Configuration.ConfigurationManager.AppSettings["cookieName"];

    protected void Page_Load(object sender, EventArgs e)
    {
        #region NoBrowserCache
        HttpContext.Current.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
        HttpContext.Current.Response.Cache.SetValidUntilExpires(false);
        HttpContext.Current.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.Cache.SetNoStore();
        #endregion
        
        //Check cookies
        if (!fnIsTokenValid(cookiesName))
        {
            Response.Redirect("./");
        }
        else
        {
            if (!IsPostBack) //First Loads
            {
                if (!string.IsNullOrEmpty(Request.QueryString["poll"]))
                {
                    string vchQSPollId = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Request.QueryString["poll"]);
                    int viPollId = 0;
                    int.TryParse(vchQSPollId, out viPollId);
                    using (dsPollTableAdapters.taPoll otaPoll = new dsPollTableAdapters.taPoll())
                    {
                        dsPoll.dtPollDataTable odtPoll = otaPoll.fnPollById(viPollId);
                        if (odtPoll.Count > 0)
                        {
                            string bannerDirectoryPath = "../upload/banner/";
                            string btnDirectoryPath = "../upload/btn/";

                            hiddPollId.Value = odtPoll[0].viId.ToString();
                            txtPollname.Text = odtPoll[0].vchName;
                            ltrQuestionLink.Text = "<a href=\"question-management.aspx?poll=" + odtPoll[0].viId.ToString() + "\" class=\"cd-btn\">Questions of this Poll</a>";
                            txtFbPageID.Text = odtPoll[0].vchFbPageId;
                            txtFbAppId.Text = odtPoll[0].vchFbAppId;
                            txtRemarks.Text = odtPoll[0].vchRemarks;
                            ltrBannerImg.Text = "<img alt=\"" + odtPoll[0].vchBannerFilename + "\" src=\"" + bannerDirectoryPath + odtPoll[0].vchBannerFilename + "\" class=\"uploaded-img\" />";
                            ltrVoteBtn.Text = "<img src=\"" + btnDirectoryPath + odtPoll[0].vchBtnFilename + "\" alt=\"" + odtPoll[0].vchBtnFilename + "\" />";
                            int viShowPoll = odtPoll[0].vblShowPoll ? 1 : 0;
                            ddlShowPoll.SelectedValue = viShowPoll.ToString();
                            fluplBanner.CssClass = "validate[custom[mime]]";
                            fluplVoteBtn.CssClass = "validate[custom[mime]]";
                        }
                        else //Record not found
                        {
                            Response.Redirect("admin_.aspx");
                        }
                    }
                }
                else
                {
                    fluplBanner.CssClass = "validate[required, custom[mime]]";
                    fluplVoteBtn.CssClass = "validate[required, custom[mime]]";
                }
            }
        }
    }

    //Check whether cookies is created
    protected bool fnIsCookieCreated(string cookiesName)
    {
        HttpCookie cookie = Request.Cookies[cookiesName];
        if (cookie != null)
            return true;
        else
            return false;
    }

    //Check for valid authentication
    protected bool fnIsTokenValid(string cookieName)
    {
        if (fnIsCookieCreated(cookieName))
        {
            HttpCookie cookie = Request.Cookies[cookieName];
            using (dsAdminTableAdapters.taAdmin otaAdmin = new dsAdminTableAdapters.taAdmin())
            {
                dsAdmin.dtAdminDataTable odtAdmin = otaAdmin.fnGetUsernameByUsername(cookie["username"]);
                if (odtAdmin.Count > 0)
                {
                    if (cookie["token"] == string.Concat(odtAdmin[0].vchUsername, odtAdmin[0].vchPassword))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            return false;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string vchPollId = string.Empty;
        if (!string.IsNullOrEmpty(hiddPollId.Value))
            vchPollId = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(hiddPollId.Value);
        int viPollId = 0;
        int.TryParse(vchPollId, out viPollId);

        #region Validation

        if (!string.IsNullOrEmpty(vchPollId))
        {
            if (string.IsNullOrEmpty(txtPollname.Text) || (ddlShowPoll.SelectedIndex == 0))
            {
                ltrErr.Text = "<span style=\"color: red;\">Submitted form is invalid, please submit a complete and valid form.</span>";
                loadingDiv.Style.Add("display", "none");
                return;
            }
        }
        else
        {
            if (string.IsNullOrEmpty(txtPollname.Text) || (ddlShowPoll.SelectedIndex == 0) || (!fluplBanner.HasFile) || (!fluplVoteBtn.HasFile))
            {
                ltrErr.Text = "<span style=\"color: red;\">Submitted form is invalid, please submit a complete and valid form.</span>";
                loadingDiv.Style.Add("display", "none");
                return;
            }
        }

        if (fluplBanner.HasFile)
        {
            //IMG check
            // DICTIONARY OF ALL IMAGE FILE HEADER
            //Dictionary<string, byte[]> imageHeader = new Dictionary<string, byte[]>();
            //imageHeader.Add("JPG", new byte[] { 0xFF, 0xD8, 0xFF, 0xE0 });
            //imageHeader.Add("JPEG", new byte[] { 0xFF, 0xD8, 0xFF, 0xE0 });
            //imageHeader.Add("PNG", new byte[] { 0x89, 0x50, 0x4E, 0x47 });

            //byte[] header;
            string bannerFileExt = fluplBanner.FileName.Substring(fluplBanner.FileName.LastIndexOf('.') + 1).ToUpper();
            string[] validFileTypes = { "PNG", "JPG", "JPEG" };
            bool isValidFile = false;
            for (int i = 0; i < validFileTypes.Length; i++)
            {
                if (bannerFileExt == validFileTypes[i])
                {
                    isValidFile = true;
                    break;
                }
            }

            //Get Header info of uploaded file (Banner)
            //byte[] tmp = imageHeader[bannerFileExt];
            //header = new byte[tmp.Length];
            //fluplBanner.FileContent.Read(header, 0, header.Length);

            if (!isValidFile)
            {
                ltrErr.Text = "<span style=\"color: red;\">Invalid submitted filetype for Banner, please submit a valid image file.</span>";
                loadingDiv.Style.Add("display", "none");
                return;
            }
            //if (!fnCompareArray(tmp, header))
            //{
            //    ltrErr.Text = "<span style=\"color: red;\">Invalid submitted filetype for Banner, please submit a valid image file.</span>";
            //    return;
            //}
        }

        if (fluplVoteBtn.HasFile)
        {
            string btnFileExt = fluplVoteBtn.FileName.Substring(fluplVoteBtn.FileName.LastIndexOf('.') + 1).ToUpper();
            string[] validFileTypes = { "PNG", "JPG", "JPEG" };
            bool isValidFile = false;
            for (int i = 0; i < validFileTypes.Length; i++)
            {
                if (btnFileExt == validFileTypes[i])
                {
                    isValidFile = true;
                    break;
                }
            }

            if (!isValidFile)
            {
                ltrErr.Text = "<span style=\"color: red;\">Invalid submitted filetype for button image, please submit a valid image file.</span>";
                loadingDiv.Style.Add("display", "none");
                return;
            }
        }

        #endregion

        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        string vchPollName = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtPollname.Text);
        string vchShowPoll = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(ddlShowPoll.SelectedValue);
        bool vblShowPoll = false;
        if (vchShowPoll == "1")
            vblShowPoll = true;
        string vchFBPageId = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtFbPageID.Text);
        string vchFbAppId = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtFbAppId.Text);
        string vchRemarks = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtRemarks.Text);
        string vchBannerFilename = System.Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10) + DateTime.Now.ToString("yyyyMMddHHmmss") + Path.GetExtension(fluplBanner.PostedFile.FileName);
        string vchBtnFilename = System.Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10) + DateTime.Now.ToString("yyyyMMddHHmmss") + Path.GetExtension(fluplVoteBtn.PostedFile.FileName);

        string bannerDirectoryPath = Directory.GetParent(Directory.GetParent(Request.PhysicalPath).FullName) + "\\upload\\banner";
        string btnDirectoryPath = Directory.GetParent(Directory.GetParent(Request.PhysicalPath).FullName) + "\\upload\\btn";

        if (fluplBanner.HasFile)
        {
            if (!Directory.Exists(bannerDirectoryPath))
            {
                Directory.CreateDirectory(bannerDirectoryPath);
            }
            fluplBanner.SaveAs(bannerDirectoryPath + "\\" + vchBannerFilename);
        }

        if (fluplVoteBtn.HasFile)
        {
            if (!Directory.Exists(btnDirectoryPath))
            {
                Directory.CreateDirectory(btnDirectoryPath);
            }
            fluplVoteBtn.SaveAs(btnDirectoryPath + "\\" + vchBtnFilename);
        }

        try
        {
            using (dsPollTableAdapters.taPoll otaPoll = new dsPollTableAdapters.taPoll())
            {
                if (!string.IsNullOrEmpty(vchPollId)) //UPDATE
                {
                    dsPoll.dtPollDataTable odtPoll = otaPoll.fnPollById(viPollId);
                    if (odtPoll.Count > 0)
                    {
                        if (fluplBanner.HasFile && fluplVoteBtn.HasFile)
                        {
                            if (otaPoll.fnUpdPollById(vchPollName, vchBannerFilename, vchBtnFilename, vblShowPoll, vchFBPageId, vchRemarks,vchFbAppId, viPollId) > 0)
                            {
                                ltrMsg.Text = "Success: Poll updated. ";
                                if (File.Exists(bannerDirectoryPath + "\\" + odtPoll[0].vchBannerFilename))
                                    File.Delete(bannerDirectoryPath + "\\" + odtPoll[0].vchBannerFilename);
                                if (File.Exists(btnDirectoryPath + "\\" + odtPoll[0].vchBtnFilename))
                                    File.Delete(btnDirectoryPath + "\\" + odtPoll[0].vchBtnFilename);
                                loadingDiv.Style.Add("display", "none");
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "click", "<script>$(document).ready(function popup() {  $('.cd-popup').addClass('is-visible'); });</script>", false);
                            }
                            else
                            {
                                ltrMsg.Text = "Error: Failed to update poll.";
                                if (File.Exists(bannerDirectoryPath + "\\" + vchBannerFilename))
                                    File.Delete(bannerDirectoryPath + "\\" + vchBannerFilename);
                                if (File.Exists(btnDirectoryPath + "\\" + vchBtnFilename))
                                    File.Delete(btnDirectoryPath + "\\" + vchBtnFilename);
                                loadingDiv.Style.Add("display", "none");
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "click", "<script>$(document).ready(function popup() {  $('.cd-popup').addClass('is-visible'); });</script>", false);
                            }
                        }
                        else if (fluplBanner.HasFile)
                        {
                            if (otaPoll.fnUpdPollBanner(vchPollName, vchBannerFilename, vblShowPoll, vchFBPageId, vchRemarks, vchFbAppId, viPollId) > 0)
                            {
                                ltrMsg.Text = "Success: Poll updated. ";
                                if (File.Exists(bannerDirectoryPath + "\\" + odtPoll[0].vchBannerFilename))
                                    File.Delete(bannerDirectoryPath + "\\" + odtPoll[0].vchBannerFilename);
                                loadingDiv.Style.Add("display", "none");
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "click", "<script>$(document).ready(function popup() { $('.cd-popup').addClass('is-visible'); });</script>", false);
                            }
                            else
                            {
                                ltrMsg.Text = "Error: Failed to update poll.";
                                if (File.Exists(bannerDirectoryPath + "\\" + vchBannerFilename))
                                    File.Delete(bannerDirectoryPath + "\\" + vchBannerFilename);
                                loadingDiv.Style.Add("display", "none");
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "click", "<script>$(document).ready(function popup() {  $('.cd-popup').addClass('is-visible'); });</script>", false);
                            }
                        }
                        else if (fluplVoteBtn.HasFile)
                        {
                            if (otaPoll.fnUpdPollBtnById(vchPollName, vchBtnFilename, vblShowPoll, vchFBPageId, vchRemarks, vchFbAppId, viPollId) > 0)
                            {
                                ltrMsg.Text = "Success: Poll updated. ";
                                if (File.Exists(btnDirectoryPath + "\\" + odtPoll[0].vchBtnFilename))
                                    File.Delete(btnDirectoryPath + "\\" + odtPoll[0].vchBtnFilename);
                                loadingDiv.Style.Add("display", "none");
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "click", "<script>$(document).ready(function popup() {  $('.cd-popup').addClass('is-visible'); });</script>", false);
                            }
                            else
                            {
                                ltrMsg.Text = "Error: Failed to update poll.";
                                if (File.Exists(btnDirectoryPath + "\\" + vchBtnFilename))
                                    File.Delete(btnDirectoryPath + "\\" + vchBtnFilename);
                                loadingDiv.Style.Add("display", "none");
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "click", "<script>$(document).ready(function popup() {  $('.cd-popup').addClass('is-visible'); });</script>", false);
                            }
                        }
                        else
                        {
                            if (otaPoll.fnUpdPollWOUpld(vchPollName, vblShowPoll, vchFBPageId, vchRemarks, vchFbAppId, viPollId) > 0)
                            {
                                ltrMsg.Text = "Success: Poll updated.";
                                loadingDiv.Style.Add("display", "none");
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "click", "<script>$(document).ready(function popup() {  $('.cd-popup').addClass('is-visible'); });</script>", false);
                            }
                            else
                            {
                                ltrMsg.Text = "Error: Failed to update poll.";
                                loadingDiv.Style.Add("display", "none");
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "click", "<script>$(document).ready(function popup() {  $('.cd-popup').addClass('is-visible'); });</script>", false);
                            }
                        }
                    }
                    else
                    {
                        ltrMsg.Text = "Fatal: Record not found, unable to update poll.";
                        loadingDiv.Style.Add("display", "none");
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "click", "<script>$(document).ready(function popup() {  $('.cd-popup').addClass('is-visible'); });</script>", false);
                    }
                }
                else //INSERT
                {
                    if (otaPoll.fnInsPoll(vchPollName, vchBannerFilename, vchBtnFilename, vblShowPoll, vchFBPageId, vchRemarks, DateTime.Now, vchFbAppId) > 0)
                    {
                        ltrMsg.Text = "New poll created. ";
                        loadingDiv.Style.Add("display", "none");
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "click", "<script>$(document).ready(function popup() {  $('.cd-popup').addClass('is-visible'); });</script>", false);
                    }
                    else
                    {
                        ltrMsg.Text = "Error: Failed to create poll.";
                        if (File.Exists(bannerDirectoryPath + "\\" + vchBannerFilename))
                            File.Delete(bannerDirectoryPath + "\\" + vchBannerFilename);
                        if (File.Exists(btnDirectoryPath + "\\" + vchBtnFilename))
                            File.Delete(btnDirectoryPath + "\\" + vchBtnFilename);
                        loadingDiv.Style.Add("display", "none");
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "click", "<script>$(document).ready(function popup() {  $('.cd-popup').addClass('is-visible'); });</script>", false);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ltrErr.Text = "<span style=\"color: red;\">" + ex.Message.ToString() + "</span>";
        }
    }

    private bool fnCompareArray(byte[] a1, byte[] a2)
    {
        if (a1.Length != a2.Length)
        {
            return false;
        }

        for (int i = 0; i < a1.Length; i++)
        {
            if (a1[i] != a2[i])
                return false;
        }
        return true;
    }

    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        string vchPollId = string.Empty;
        if (!string.IsNullOrEmpty(hiddPollId.Value))
            vchPollId = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(hiddPollId.Value);
        if (!string.IsNullOrEmpty(vchPollId))
        {
            Response.Redirect(Request.RawUrl);
        }
        else
        {
            Response.Redirect("admin_.aspx");
        }
    }

    protected void lnkBtnLogout_Click(object sender, EventArgs e)
    {
        if (fnIsTokenValid(cookiesName))
        {
            //Start destroying cookie
            HttpCookie cookie = Request.Cookies[cookiesName];
            DateTime dt = DateTime.Now.AddYears(-1);
            cookie.Expires = dt;
            Response.Cookies.Add(cookie);
            Response.Redirect("./");
        }
        else
        {
            Response.Redirect("./");
        }
    }
}