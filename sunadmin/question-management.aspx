﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="question-management.aspx.cs" Inherits="sunadmin_question_management" %>

<%@ Register Assembly="System.Web.Entity, Version=3.5.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>[Admin]Question Management</title>
    <meta name="robots" content="noindex, nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- For IE 9 and below. ICO should be 32x32 pixels in size -->
    <!--[if IE]><link rel="shortcut icon" href="../favicon.ico"><![endif]-->

    <!-- Touch Icons - iOS and Android 2.1+ 180x180 pixels in size. -->
    <link rel="apple-touch-icon-precomposed" href="../images/astro_.png">

    <!-- Firefox, Chrome, Safari, IE 11+ and Opera. 196x196 pixels in size. -->
    <link rel="icon" href="../favicon.ico">
    <link href='//fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link href="../css/main.css" rel="stylesheet" type="text/css" />
    <link href="//cdn.datatables.net/1.10.5/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        table.dataTable tr.odd { background-color: #efefef; } 
        table.dataTable tr.even { background-color: white; }
    </style>
</head>
<body>
    <form id="frmMain" runat="server">
        <h1 style="font-size: 2.125em; color: black;">
            <asp:Literal ID="ltrTitle" runat="server"></asp:Literal>
        </h1>
        <p>
            <a href="admin_.aspx" class="cd-btn">< Back</a>
            <asp:Literal ID="ltrAddQues" runat="server"></asp:Literal>
            <asp:LinkButton ID="lnkBtnLogout" class="cd-btn" runat="server" OnClick="btnLogout_Click">Logout</asp:LinkButton>
        </p>
        <div>
            <table id="questionListing" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="no-sort" style="text-align: right;">#</th>
                        <th>Question</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Is Vote Capped?</th>
                        <th>Result JSON Feed</th>
                        <th>Created On</th>
                        <th class="no-sort">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:ListView ID="lstvwQuestions" runat="server" EnableModelValidation="True">
                        <EmptyDataTemplate>
                            <span>No data was returned.</span>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr>
                                <td style="text-align: right;"><%# ((ListViewDataItem)Container).DataItemIndex +1 %></td>
                                <td><%# Eval("vchQuestiontext") %></td>
                                <td style="text-align: center;"><%# Eval("vdtPollStart", "{0:MMM dd, yyyy}") %></td>
                                <td style="text-align: center;"><%# Eval("vdtPollEnd", "{0:MMM dd, yyyy}") %></td>
                                <td style="text-align: center;"><%# Eval("vblIsVoteCapped").ToString().ToUpper() == "TRUE" ? Eval("viVoteLimit") : "NO" %></td>
                                <td style="text-align: center;"><a href="../result.ashx?pid=<%# Eval("viPollIdFk") %>&qid=<%# Eval("vchQuestionId") %>" target="_blank">Result URL</a></td>
                                <td style="text-align: center;"><%# Eval("vdtCreateddatetime", "{0:MMM dd, yyyy hh:mm tt}") %></td>
                                <td style="text-align: center;"><a class="btn-front2" target="_blank" href="preview.aspx?pid=<%# Eval("viPollIdFk") %>&qid=<%# Eval("vchQuestionId") %>">Preview</a> | <a class="btn-front2" href="question.aspx?poll=<%# Eval("viPollIdFk") %>&question=<%# Eval("vchQuestionId") %>">Edit</a> | <a class="btn-front2" href="stats.aspx?pid=<%# Eval("viPollIdFk") %>&qid=<%# Eval("vchQuestionId") %>">Stats</a> | <a class="btn-front" data-poll="<%# Eval("viPollIdFk") %>" data-question="<%# Eval("vchQuestionId") %>">Delete</a></td>
                            </tr>
                        </ItemTemplate>
                        <LayoutTemplate>
                            <span runat="server" id="itemPlaceholder" />
                        </LayoutTemplate>
                    </asp:ListView>
                </tbody>
            </table>
        </div>
        <div class="cd-popup" role="alert">
            <div class="cd-popup-container">
                <p>Are you sure you want to delete this?</p>
                <ul class="cd-buttons">
                    <li style='width: 50%;'><a class='cd-buttons-close'>No</a></li>
                    <li style='width: 50%;'><a class='cd-buttons-yes'>Yes</a></li>
                </ul>
                <a class="cd-popup-close img-replace" style="cursor: pointer;">Close</a>
            </div>
        </div>
        <asp:HiddenField ID="hiddPollId" runat="server" />
        <asp:HiddenField ID="hiddQuestionId" runat="server" />
        <div id="loadingDiv" style="display: none;">
            <img src="../images/bold.gif" alt="Loading" />
        </div>
        <div style="display: none;">
            <asp:Button ID="btnDlt" runat="server" Text="Delete" OnClick="btnDlt_Click" />
        </div>
    </form>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#questionListing').DataTable({ columnDefs: [{ targets: 'no-sort', orderable: false }], "aaSorting": [] });

            $(document).on('click', '.btn-front', function (event) {
                event.preventDefault();
                $('#<%=hiddPollId.ClientID%>').val($(this).attr('data-poll'));
                $('#<%=hiddQuestionId.ClientID%>').val($(this).attr('data-question'));
                $('.cd-popup').addClass('is-visible');
            });

            //close popup
            $('.cd-popup').on('click', function (event) {
                if ($(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup') || $(event.target).is('.cd-buttons-close')) {
                    event.preventDefault();
                    $(this).removeClass('is-visible');
                    $('#<%=hiddPollId.ClientID%>').val('');
                    $('#<%=hiddQuestionId.ClientID%>').val('');
                }
            });

            //close popup
            $('.cd-popup').on('click', function (event) {
                if ($(event.target).is('.cd-buttons-yes')) {
                    event.preventDefault();
                    $("#<%=btnDlt.ClientID%>").trigger("click");
                    $(this).removeClass('is-visible');
                }
            });

            $('#<%=btnDlt.ClientID%>').on('click', function (event) {
                var docHeight = $(document).height();
                $("#loadingDiv").height(docHeight)
                .css({
                    'opacity': 0.85,
                    'position': 'absolute',
                    'top': 0,
                    'left': 0,
                    'background-color': 'white',
                    'width': '100%',
                    'z-index': 5000
                });

                $('#loadingDiv').show();
            });
        });
    </script>
</body>
</html>
