﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="sunadmin_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Poll Management</title>
    <meta name="robots" content="noindex, nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- For IE 9 and below. ICO should be 32x32 pixels in size -->
    <!--[if IE]><link rel="shortcut icon" href="../favicon.ico"><![endif]-->

    <!-- Touch Icons - iOS and Android 2.1+ 180x180 pixels in size. -->
    <link rel="apple-touch-icon-precomposed" href="../images/astro_.png">

    <!-- Firefox, Chrome, Safari, IE 11+ and Opera. 196x196 pixels in size. -->
    <link rel="icon" href="../favicon.ico">
    <link href="../css/login.css" rel="stylesheet" type="text/css" />
    <link href="../css/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="cont">
        <div class="demo">
            <div class="login">
                <div class="login__check"></div>
                <form id="frmMain" runat="server">
                    <div class="login__form">
                        <div class="login__row">
                            <svg class="login__icon name svg-icon" viewBox="0 0 20 20">
                                <path d="M0,20 a10,8 0 0,1 20,0z M10,0 a4,4 0 0,1 0,8 a4,4 0 0,1 0,-8" />
                            </svg>
                            <asp:TextBox ID="txtUsername" autocomplete="off" placeholder="Username" CssClass="login__input name validate[required]" runat="server"></asp:TextBox>
                        </div>
                        <div class="login__row">
                            <svg class="login__icon pass svg-icon" viewBox="0 0 20 20">
                                <path d="M0,20 20,20 20,8 0,8z M10,13 10,16z M4,8 a6,8 0 0,1 12,0" />
                            </svg>
                            <asp:TextBox ID="txtPassword" autocomplete="off" placeholder="Password" CssClass="login__input pass validate[required]" runat="server" TextMode="Password"></asp:TextBox>
                        </div>
                        <div class="login__row">
                            <div class="g-recaptcha" style="transform:scale(0.84);transform-origin:0 0" data-sitekey="<asp:Literal runat='server' Text='<%$ appSettings:recaptcha:publicKey%>' />"></div>
                        </div>
                        <button type="button" id="signinBtn" class="login__submit">Sign In</button>
                        <div style="display: none;">
                            <asp:Button ID="btnSubmit" runat="server" Text="Sign In" OnClick="btnSubmit_Click" />
                        </div>
                        <p class="login__signup">
                            <asp:Label ID="lblMsg" runat="server"></asp:Label>
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <%--<div id="wrapper">
        <form id="frmMain" class="login-form" runat="server">
            <div class="header">
                <h1>[Login] Poll Management</h1>
                <span>
                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                </span>
            </div>
            <div class="content">
                <asp:TextBox ID="txtUsername" placeholder="Username" CssClass="input username validate[required]" runat="server"></asp:TextBox>
                <div class="user-icon"></div>
                <div class="margin20">&nbsp;</div>
                <asp:TextBox ID="txtPassword" placeholder="Password" CssClass="input password validate[required]" runat="server" TextMode="Password"></asp:TextBox>
                <div class="pass-icon"></div>
            </div>
            <div class="footer">
                <asp:Button ID="btnSubmit" CssClass="button" runat="server" Text="Login" OnClick="btnSubmit_Click" />
            </div>
        </form>
    </div>--%>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="../js/jquery.validationEngine-en.js" type="text/javascript"></script>
    <script src="../js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
    <script src="//www.google.com/recaptcha/api.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#frmMain").validationEngine('attach', { promptPosition: "topLeft" });

            var animating = false;

            function ripple(elem, e) {
                $(".ripple").remove();
                var elTop = elem.offset().top,
                    elLeft = elem.offset().left,
                    x = e.pageX - elLeft,
                    y = e.pageY - elTop;
                var $ripple = $("<div class='ripple'></div>");
                $ripple.css({ top: y, left: x });
                elem.append($ripple);
            };

            $(document).on("click", "#signinBtn", function (e) {
                if (($("#frmMain").validationEngine('validate') === true)) {
                    if (animating) return;
                    animating = true;
                    var that = this;
                    ripple($(that), e);
                    $(that).addClass("processing");
                    $("#<%=btnSubmit.ClientID%>").trigger("click");
                }
            });
        });
    </script>
</body>
</html>
