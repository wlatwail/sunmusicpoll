﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="admin_.aspx.cs" Inherits="sunadmin_admin_" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>[Admin]Poll</title>
    <meta name="robots" content="noindex, nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- For IE 9 and below. ICO should be 32x32 pixels in size -->
    <!--[if IE]><link rel="shortcut icon" href="../favicon.ico"><![endif]-->

    <!-- Touch Icons - iOS and Android 2.1+ 180x180 pixels in size. -->
    <link rel="apple-touch-icon-precomposed" href="../images/astro_.png">

    <!-- Firefox, Chrome, Safari, IE 11+ and Opera. 196x196 pixels in size. -->
    <link rel="icon" href="../favicon.ico">
    <link href='//fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link href="../css/main.css" rel="stylesheet" type="text/css" />
    <link href="//cdn.datatables.net/1.10.5/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="frmMain" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <p>
            <a href="poll-management.aspx" class="cd-btn">Create Poll</a>
            <asp:LinkButton ID="lnkBtnLogout" class="cd-btn" runat="server" OnClick="btnLogout_Click">Logout</asp:LinkButton>
        </p>
        <div>
            <table id="pollListing" class="display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="no-sort" style="text-align: right;">#</th>
                        <th>Poll Name</th>
                        <th>Public / Private</th>
                        <th>FB Page ID</th>
                        <th>Created On</th>
                        <th class="no-sort">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:ListView ID="lstvwPollListing" runat="server" DataKeyNames="viId">
                        <EmptyDataTemplate>
                            <span>No available poll.</span>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr>
                                <td style="text-align: right;"><%# ((ListViewDataItem)Container).DataItemIndex +1 %></td>
                                <td><%# Eval("vchName") %></td>
                                <td style="text-align: center;"><%# Eval("vblShowPoll").ToString().ToUpper() == "TRUE" ? "Public" : "Private" %></td>
                                <td style="text-align: right;"><a href="https://www.facebook.com/pages/-/<%# Eval("vchFbPageId") %>" target="_blank"><%# Eval("vchFbPageId") %></a></td>
                                <td style="text-align: center;"><%# Eval("vdtCreateddatetime", "{0:MMM dd, yyyy hh:mm tt}") %></td>
                                <td style="text-align: center;">
                                    <a class="btn-front2" target="_blank" href="../poll.aspx?pid=<%# Eval("viId") %>">URL</a> | <a class="btn-front2" href="poll-management.aspx?poll=<%# Eval("viId") %>">Edit</a> | <a class="btn-front2" href="question-management.aspx?poll=<%# Eval("viId") %>">Questions</a> | <a id="DltBtn<%# Eval("viId") %>" data-id="<%# Eval("viId") %>" class="btn-front">Delete</a>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <LayoutTemplate>
                            <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                        </LayoutTemplate>
                    </asp:ListView>
                </tbody>
            </table>
        </div>
        <div class="cd-popup" role="alert">
            <div class="cd-popup-container">
                <p>Are you sure you want to delete this?</p>
                <ul class="cd-buttons">
                    <li style='width: 50%;'><a class='cd-buttons-close'>No</a></li>
                    <li style='width: 50%;'><a class='cd-buttons-yes'>Yes</a></li>
                </ul>
                <a class="cd-popup-close img-replace" style="cursor: pointer;">Close</a>
            </div>
        </div>
        <asp:HiddenField ID="hiddPollId" runat="server" />
        <div style="display: none;">
            <asp:Button ID="btnDlt" runat="server" Text="Delete" OnClick="btnDlt_Click" />
        </div>
        <div id="loadingDiv" style="display: none;">
            <img src="../images/bold.gif" alt="Loading" />
        </div>
    </form>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#pollListing').DataTable({ columnDefs: [{ targets: 'no-sort', orderable: false }], "aaSorting": [] });

            $(document).on('click', '.btn-front', function (event) {
                event.preventDefault();
                $('#<%=hiddPollId.ClientID%>').val($(this).attr('data-id'));
                $('.cd-popup').addClass('is-visible');
            });

            //close popup
            $('.cd-popup').on('click', function (event) {
                if ($(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup') || $(event.target).is('.cd-buttons-close')) {
                    event.preventDefault();
                    $(this).removeClass('is-visible');
                    $('#<%=hiddPollId.ClientID%>').val('');
                }
            });

            //close popup
            $('.cd-popup').on('click', function (event) {
                if ($(event.target).is('.cd-buttons-yes')) {
                    event.preventDefault();
                    $("#<%=btnDlt.ClientID%>").trigger("click");
                    $(this).removeClass('is-visible');
                }
            });

            $('#<%=btnDlt.ClientID%>').on('click', function (event) {
                var docHeight = $(document).height();
                $("#loadingDiv").height(docHeight)
                .css({
                    'opacity': 0.85,
                    'position': 'absolute',
                    'top': 0,
                    'left': 0,
                    'background-color': 'white',
                    'width': '100%',
                    'z-index': 5000
                });

                $('#loadingDiv').show();
            });
        });
    </script>
</body>
</html>
