﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="preview.aspx.cs" Inherits="sunadmin_preview" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="robots" content="noindex, nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- For IE 9 and below. ICO should be 32x32 pixels in size -->
    <!--[if IE]><link rel="shortcut icon" href="../favicon.ico"><![endif]-->

    <!-- Touch Icons - iOS and Android 2.1+ 180x180 pixels in size. -->
    <link rel="apple-touch-icon-precomposed" href="../images/astro_.png">

    <!-- Firefox, Chrome, Safari, IE 11+ and Opera. 196x196 pixels in size. -->
    <link rel="icon" href="../favicon.ico">
    <title>[Preview]<asp:Literal ID="ltrTitle" runat="server"></asp:Literal></title>
    <link href="../css/normalize.css" rel="stylesheet" />
    <link href="../css/poll.css" rel="stylesheet" />
</head>
<body>
    <div id="fb-root">
    </div>
    <form id="frmMain" runat="server" class="cd-form">
        <div id="parent" runat="server">
            <div id="topbanner">
                <asp:Literal ID="ltrBannerImg" runat="server"></asp:Literal>
            </div>
            <div id="midbannerparent">
                <div id="divFblogin" style="display: none;">
                    <!-- FB Login-->
                    Get connected and start voting now. <br />
                    <a id="btnFbLogin" class="btn-auth btn-facebook large">Facebook Connect</a>
                    <!-- End of FB Login-->
                </div>
                <div id="midbanner">
                    <div style="padding-left: 10%;" id="shareWrapperDiv" runat="server">
                        <div class="fb-icon-bg"></div>
                        <div class="fb-bg" id="fbsharediv"></div>
                    </div>
                    <span id="title">
                        <asp:Literal ID="ltrVoteTitle" runat="server"></asp:Literal>
                    </span>
                    <hr style="width: 80%; align-self: center">
                   <asp:Literal ID="ltrViewResult" runat="server"></asp:Literal>
                    <p id="question">
                        <asp:Literal ID="ltrQuestion" runat="server"></asp:Literal>
                    </p>
                    <div id="form">
                        <asp:RadioButtonList ID="radbtnlstChoices" CssClass="cd-form-list" DataTextField="vchAnswertext" DataValueField="viId" runat="server" RepeatLayout="UnorderedList" RepeatDirection="Vertical"></asp:RadioButtonList>
                    </div>
                    <div id="submit">
                        <asp:Literal ID="ltrSubmitBtn" runat="server"></asp:Literal>
                    </div>
                    <div id="container" style="height: 400px; margin-left: auto; margin-right: auto; width: 80%; display: none"></div>
                    <hr id="submitline" style="width: 80%; align-self: center">
                    <p id="info">
                        <asp:Literal ID="ltrRemarks" runat="server"></asp:Literal>
                        <asp:HiddenField ID="hiddPollId" runat="server" />
                        <asp:HiddenField ID="hiddQuesId" runat="server" />
                    </p>
                    <!--facebook-like-->
                    <div style="font-size: medium; padding: 0% 0% 1% 10%;">
                        Don't forget to 'Like' our page and stay tune for upcoming updates!
                        <asp:Literal ID="ltrFBLike" runat="server"></asp:Literal>
                    </div>
                    <!-- end FB like -->
                </div>
            </div>
            <div id="footer">
                <p id="note">Copyright <%=DateTime.Now.Year %> Astro Interactive</p>
            </div>
        </div>
    </form>
    <!-- JavaScript at the bottom for fast page loading -->
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>  
    <script type="text/javascript">
        // Load the SDK asynchronously
        (function (d) {
            var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
            if (d.getElementById(id)) { return; }
            js = d.createElement('script'); js.id = id; js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";
            ref.parentNode.insertBefore(js, ref);
        }(document));
    </script>  
</body>
</html>
