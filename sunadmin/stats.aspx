﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="stats.aspx.cs" Inherits="sunadmin_user" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <asp:Literal ID="ltrTitle" runat="server"></asp:Literal>
    </title>
    <meta name="robots" content="noindex, nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- For IE 9 and below. ICO should be 32x32 pixels in size -->
    <!--[if IE]><link rel="shortcut icon" href="../favicon.ico"><![endif]-->

    <!-- Touch Icons - iOS and Android 2.1+ 180x180 pixels in size. -->
    <link rel="apple-touch-icon-precomposed" href="../images/astro_.png">

    <!-- Firefox, Chrome, Safari, IE 11+ and Opera. 196x196 pixels in size. -->
    <link rel="icon" href="../favicon.ico">
    <link href="../css/stats.css" rel="stylesheet" type="text/css" />
    <link href="//cdn.datatables.net/1.10.5/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        table.dataTable tr.odd {
            background-color: #efefef;
        }

        table.dataTable tr.even {
            background-color: white;
        }
    </style>
</head>
<body>
    <form id="frmMain" runat="server">
        <h1 style="font-size: 2.125em; color: black;">
            <asp:Literal ID="ltrPollTitle" runat="server"></asp:Literal>
        </h1>
        <p>
            <asp:Literal ID="ltrBack" runat="server"></asp:Literal>
            <asp:LinkButton ID="lnkBtnLogout" class="cd-btn" runat="server" OnClick="btnLogout_Click">Logout</asp:LinkButton>
        </p>
        <div>
            <fieldset>
                <legend><span style="font-size: 2.125em; color: black;">Rankings</span></legend>
                <div id="containerVoteStanding" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                <table id="voteStanding" cellspacing="0" width="60%" style="margin: 0;">
                    <thead>
                        <tr>
                            <th class="no-sort" style="text-align: right;">#</th>
                            <th style="text-align: left;">Options</th>
                            <th style="text-align: right;">Total Votes</th>
                            <th style="text-align: right;">Percentage (%)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:ListView ID="lstvwVoteStanding" runat="server" EnableModelValidation="True">
                            <EmptyDataTemplate>
                                <span>No data was returned.</span>
                            </EmptyDataTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: right;"><%# ((ListViewDataItem)Container).DataItemIndex +1 %></td>
                                    <td style="text-align: left;"><%# Eval("vchAnswertext") %></td>
                                    <td style="text-align: right;"><%# Eval("viTotal") %></td>
                                    <td style="text-align: right;"><%# Eval("Percentage", "{0:f2}%") %></td>
                                </tr>
                            </ItemTemplate>
                            <LayoutTemplate>
                                <span runat="server" id="itemPlaceholder" />
                            </LayoutTemplate>
                        </asp:ListView>
                    </tbody>
                </table>
            </fieldset>
        </div>
        <br /><br />
        <div>
            <fieldset>
                <legend><span style="font-size: 2.125em; color: black;">Received Votes (breakdown into days) </span></legend>
                <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                <table id="DailyVotesListing" cellspacing="0" width="60%" style="margin: 0;">
                    <thead>
                        <tr>
                            <th class="no-sort" style="text-align: right;">#</th>
                            <th style="text-align: left;">Date</th>
                            <th style="text-align: right;">Total Votes</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:ListView ID="lstVwDailyVotes" runat="server" EnableModelValidation="True">
                            <EmptyDataTemplate>
                                <span>No data was returned.</span>
                            </EmptyDataTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: right;"><%# ((ListViewDataItem)Container).DataItemIndex +1 %></td>
                                    <td style="text-align: left;"><%# Eval("vdtDate", "{0:MMM dd, yyyy}") %></td>
                                    <td style="text-align: right;"><%# Eval("viTotal") %></td>
                                </tr>
                            </ItemTemplate>
                            <LayoutTemplate>
                                <span runat="server" id="itemPlaceholder" />
                            </LayoutTemplate>
                        </asp:ListView>
                    </tbody>
                </table>
            </fieldset>
        </div>
        <br />
        <br />
        <div>
            <fieldset>
                <legend><span style="font-size: 2.125em; color: black;">Top Voters (In Descending Order): </span></legend>
                <span style="font-size: 1.125em; color: black; border: solid 0.15em black; padding: 0.5em; display: inline-block; background: aliceblue;">Total Received Votes:
                    <asp:Literal ID="ltrTotalVotesReceived" runat="server"></asp:Literal></span>
                <table id="VotersListing" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="no-sort" style="text-align: right;">#</th>
                            <th class="no-sort">Profile Picture</th>
                            <th>FB ID</th>
                            <th style="text-align: left;">FB Name</th>
                            <th style="text-align: left;">FB URL</th>
                            <th style="text-align: left;">Email Address</th>
                            <th style="text-align: right;">Total Votes</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:ListView ID="lstVwUniqueVotes" runat="server" EnableModelValidation="True">
                            <EmptyDataTemplate>
                                <span>No data was returned.</span>
                            </EmptyDataTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: right;"><%# ((ListViewDataItem)Container).DataItemIndex +1 %></td>
                                    <td style="text-align: center;">
                                        <img alt="<%# Eval("vchFbid") %>" src="https://graph.facebook.com/<%# Eval("vchFbid") %>/picture?type=large" style="max-width: 50px;" />
                                    </td>
                                    <td style="text-align: right;">
                                        <a href="https://facebook.com/profile.php?id=<%# Eval("vchFbid") %>" target="_blank" rel="nofollow"><%# Eval("vchFbid") %></a>
                                    </td>
                                    <td style="text-align: left;"><%# Eval("vchFbname") %></td>
                                    <td style="text-align: left;"><a href='<%# Eval("vchFblink") %>' target="_blank" rel="nofollow">Link</a></td>
                                    <td style="text-align: left;"><a href='mailto:<%# HttpUtility.UrlDecode((string)Eval("vchEmail")) %>'><%# HttpUtility.UrlDecode((string)Eval("vchEmail")) %></a></td>
                                    <td style="text-align: right;"><%# Eval("viTotal") %></td>
                                </tr>
                            </ItemTemplate>
                            <LayoutTemplate>
                                <span runat="server" id="itemPlaceholder" />
                            </LayoutTemplate>
                        </asp:ListView>
                    </tbody>
                </table>
            </fieldset>
        </div>
        <asp:HiddenField ID="hiddPollId" runat="server" />
        <asp:HiddenField ID="hiddQuestionId" runat="server" />
    </form>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
    <script src="../js/highcharts.js" type="text/javascript" charset="UTF-8"></script>
    <script src="../js/highcharts-3d.js" type="text/javascript" charset="UTF-8"></script>
    <script>
        $(document).ready(function () {
            $('#VotersListing').DataTable({ columnDefs: [{ targets: 'no-sort', orderable: false }], "aaSorting": [] });
            $('#DailyVotesListing').DataTable({ columnDefs: [{ targets: 'no-sort', orderable: false }], "aaSorting": [] });

            var jsonQuery = {
                'pid': $('#<%=hiddPollId.ClientID%>').val(),
                'qid': $('#<%=hiddQuestionId.ClientID%>').val()
            }
            var chartdata = [];
            $.ajax({
                url: "../chartresult.ashx/ProcessRequest",
                cache: false,
                type: 'POST',
                data: jsonQuery,
                async: false,
                beforeSend: function () {
                },
                success: function (response) {
                    if (response.chart_type === 'pie') {
                        var arr = response.options;
                        for (var i = 0; i < arr.length; i++) {
                            temparray = [arr[i].option_text, arr[i].total_percentage];
                            chartdata.push(temparray);
                        }
                        var chart = new Highcharts.Chart({
                            chart: {
                                renderTo: 'containerVoteStanding',
                                type: 'pie',
                                options3d: {
                                    enabled: true,
                                    alpha: 45,
                                    beta: 0
                                }
                            },
                            title: {
                                text: null
                            },
                            tooltip: {
                                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                            },
                            plotOptions: {
                                pie: {
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    depth: 35,
                                    dataLabels: {
                                        enabled: true,
                                        format: '{point.name}: {point.percentage:.1f}%',
                                        distance: 20
                                    }
                                }
                            },
                            credits: {
                                enabled: false
                            },
                            series: [{
                                type: 'pie',
                                name: 'Percentage',
                                data: chartdata
                            }]
                        });
                    }
                    else if (response.chart_type === 'bar') {
                        var arr = response.options;
                        var chartXaxis = [];
                        for (var i = 0; i < arr.length; i++) {
                            temparray = [arr[i].option_text, arr[i].total_percentage];
                            chartdata.push(temparray);
                            temparray = [arr[i].option_text];
                            chartXaxis.push(temparray);
                        }
                        var chart = new Highcharts.Chart({
                            chart: {
                                renderTo: 'containerVoteStanding',
                                type: 'bar'
                            },
                            title: {
                                text: null
                            },
                            xAxis: {
                                categories: chartXaxis,
                                title: {
                                    text: null
                                }
                            },
                            yAxis: {
                                min: 0,
                                max: 100,
                                title: {
                                    text: null,
                                    align: 'high'
                                },
                                labels: {
                                    overflow: 'justify'
                                }
                            },
                            tooltip: {
                                valueSuffix: ' %'
                            },
                            plotOptions: {
                                bar: {
                                    dataLabels: {
                                        enabled: true,
                                        format: '{point.y:.2f}%'
                                    }
                                }
                            },
                            credits: {
                                enabled: false
                            },
                            series: [
                                {
                                    name: 'Percentage',
                                    data: chartdata
                                }
                            ]
                        });
                    }
                    else {
                        $('#containerVoteStanding').text(response.title);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.responseText);
                },
                complete: function () {
                }
            });

        });
    </script>
    <asp:Literal ID="ltrHighcharts" runat="server"></asp:Literal>
</body>
</html>
