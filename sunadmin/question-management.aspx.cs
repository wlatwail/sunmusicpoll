﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TransactionHelper;

public partial class sunadmin_question_management : System.Web.UI.Page
{
    string cookiesName = System.Configuration.ConfigurationManager.AppSettings["cookieName"];

    protected void Page_Load(object sender, EventArgs e)
    {
        #region NoBrowserCache
        HttpContext.Current.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
        HttpContext.Current.Response.Cache.SetValidUntilExpires(false);
        HttpContext.Current.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.Cache.SetNoStore();
        #endregion

        //Check cookies
        if (!fnIsTokenValid(cookiesName))
        {
            Response.Redirect("./");
        }
        else
        {
            if (!IsPostBack)  //First Loads
            {
                if (!string.IsNullOrEmpty(Request.QueryString["poll"]))
                {
                    string vchQSPollId = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Request.QueryString["poll"]);
                    int viPollId = 0;
                    int.TryParse(vchQSPollId, out viPollId);
                    using (dsPollTableAdapters.taPoll otaPoll = new dsPollTableAdapters.taPoll())
                    {
                        dsPoll.dtPollDataTable odtPoll = otaPoll.fnPollById(viPollId);
                        if (odtPoll.Count > 0)
                        {
                            ltrTitle.Text = "[" + odtPoll[0].vchName + "] Poll's Question(s)";
                            ltrAddQues.Text = "<a href=\"question.aspx?poll=" + odtPoll[0].viId + "\" class=\"cd-btn\">Add Question [+]</a>";
                            using (dsQuestionTableAdapters.taQuestionListing otaQuestionListing = new dsQuestionTableAdapters.taQuestionListing())
                            {
                                lstvwQuestions.DataSource = otaQuestionListing.fnQuestionListingByPollId(viPollId);
                                lstvwQuestions.DataBind();
                            }
                        }
                    }
                }
                else
                {
                    Response.Redirect("admin_.aspx");
                }
            }
        }

    }

    //Check whether cookies is created
    protected bool fnIsCookieCreated(string cookiesName)
    {
        HttpCookie cookie = Request.Cookies[cookiesName];
        if (cookie != null)
            return true;
        else
            return false;
    }

    //Check for valid authentication
    protected bool fnIsTokenValid(string cookieName)
    {
        if (fnIsCookieCreated(cookieName))
        {
            HttpCookie cookie = Request.Cookies[cookieName];
            using (dsAdminTableAdapters.taAdmin otaAdmin = new dsAdminTableAdapters.taAdmin())
            {
                dsAdmin.dtAdminDataTable odtAdmin = otaAdmin.fnGetUsernameByUsername(cookie["username"]);
                if (odtAdmin.Count > 0)
                {
                    if (cookie["token"] == string.Concat(odtAdmin[0].vchUsername, odtAdmin[0].vchPassword))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            return false;
        }
    }

    protected void btnLogout_Click(object sender, EventArgs e)
    {
        if (fnIsTokenValid(cookiesName))
        {
            //Start destroying cookie
            HttpCookie cookie = Request.Cookies[cookiesName];
            DateTime dt = DateTime.Now.AddYears(-1);
            cookie.Expires = dt;
            Response.Cookies.Add(cookie);
            Response.Redirect("./");
        }
        else
        {
            Response.Redirect("./");
        }
    }

    protected void btnDlt_Click(object sender, EventArgs e)
    {
        string vchPollId = string.Empty;
        string vchQuestionId = string.Empty;
        if (!string.IsNullOrEmpty(hiddPollId.Value))
            vchPollId = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(hiddPollId.Value);
        if (!string.IsNullOrEmpty(hiddQuestionId.Value))
            vchQuestionId = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(hiddQuestionId.Value);
        int viPollId = 0;
        int.TryParse(vchPollId, out viPollId);

        if ((!string.IsNullOrEmpty(vchPollId)) && (!string.IsNullOrEmpty(vchQuestionId)))
        {
            SqlTransaction transaction = null;

            try
            {
                using (dsQuestionTableAdapters.taQuestion otaQuestion = new dsQuestionTableAdapters.taQuestion())
                {
                    transaction = TableAdapterHelper.BeginTransaction(otaQuestion);
                    otaQuestion.fnDltQuestion(viPollId, vchQuestionId);
                }

                using (dsAnswerTableAdapters.taAnswer otaAnswer = new dsAnswerTableAdapters.taAnswer())
                {
                    TableAdapterHelper.SetTransaction(otaAnswer, transaction);
                    otaAnswer.fnDltAnswerByPollIDQuestionId(viPollId, vchQuestionId);
                }
                transaction.Commit();
                Response.Redirect(Request.RawUrl);
            }
            catch
            {
                transaction.Rollback();
                Response.Write("Exception caught: SQL Transaction Rolled back.");
            }
            finally
            {
                transaction.Dispose();
            }
        }
        else
        {
            Response.Redirect(Request.RawUrl);
        }
    }
}