﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class sunadmin_admin_ : System.Web.UI.Page
{
    string cookiesName = System.Configuration.ConfigurationManager.AppSettings["cookieName"];

    protected void Page_Load(object sender, EventArgs e)
    {
        #region NoBrowserCache
        HttpContext.Current.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
        HttpContext.Current.Response.Cache.SetValidUntilExpires(false);
        HttpContext.Current.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.Cache.SetNoStore();
        #endregion

        if(!IsPostBack)
        {
            //Check cookies
            if(!fnIsTokenValid(cookiesName))
            {
                Response.Redirect("./");
            }
            else
            {
                using (dsPollTableAdapters.taPoll otaPoll = new dsPollTableAdapters.taPoll())
                {
                    lstvwPollListing.DataSource = otaPoll.fnPollByDesc();
                    lstvwPollListing.DataBind();
                }
            }
        }
    }

    //Check whether cookies is created
    protected bool fnIsCookieCreated(string cookiesName)
    {
        HttpCookie cookie = Request.Cookies[cookiesName];
        if (cookie != null)
            return true;
        else
            return false;
    }

    //Check for valid authentication
    protected bool fnIsTokenValid(string cookieName)
    {
        if(fnIsCookieCreated(cookieName))
        {
            HttpCookie cookie = Request.Cookies[cookieName];
            using (dsAdminTableAdapters.taAdmin otaAdmin = new dsAdminTableAdapters.taAdmin())
            {
                dsAdmin.dtAdminDataTable odtAdmin = otaAdmin.fnGetUsernameByUsername(cookie["username"]);
                if(odtAdmin.Count > 0)
                {
                    if(cookie["token"] == string.Concat(odtAdmin[0].vchUsername,odtAdmin[0].vchPassword))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            return false;
        }
    }
    
    protected void btnLogout_Click(object sender, EventArgs e)
    {
        if (fnIsTokenValid(cookiesName))
        {
            //Start destroying cookie
            HttpCookie cookie = Request.Cookies[cookiesName];
            DateTime dt = DateTime.Now.AddYears(-1);
            cookie.Expires = dt;
            Response.Cookies.Add(cookie);
            Response.Redirect("./");
        }
        else
        {
            Response.Redirect("./");
        }
    }
    protected void btnDlt_Click(object sender, EventArgs e)
    {
        string vchPollId = string.Empty;
        if (!string.IsNullOrEmpty(hiddPollId.Value))
            vchPollId = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(hiddPollId.Value);
        int viPollId = 0;
        int.TryParse(vchPollId, out viPollId);

        if (!string.IsNullOrEmpty(vchPollId))
        {
            using (dsPollTableAdapters.taPoll otaPoll = new dsPollTableAdapters.taPoll())
            {
                dsPoll.dtPollDataTable odtPoll = otaPoll.fnPollById(viPollId);
                if (odtPoll.Count > 0)
                {
                    if (otaPoll.fnDltPollById(viPollId) > 0)
                    {
                        string bannerDirectoryPath = Directory.GetParent(Directory.GetParent(Request.PhysicalPath).FullName) + "\\upload\\banner";
                        string btnDirectoryPath = Directory.GetParent(Directory.GetParent(Request.PhysicalPath).FullName) + "\\upload\\btn";

                        if (File.Exists(bannerDirectoryPath + "\\" + odtPoll[0].vchBannerFilename))
                            File.Delete(bannerDirectoryPath + "\\" + odtPoll[0].vchBannerFilename);
                        if (File.Exists(btnDirectoryPath + "\\" + odtPoll[0].vchBtnFilename))
                            File.Delete(btnDirectoryPath + "\\" + odtPoll[0].vchBtnFilename);
                        Response.Redirect(Request.RawUrl);
                    }
                    else
                    {
                        Response.Write("Delete Failed.");
                    }
                }
                else
                {
                    Response.Redirect(Request.RawUrl);
                }
            }
        }
        else
        {
            Response.Redirect(Request.RawUrl);
        }        
    }
}