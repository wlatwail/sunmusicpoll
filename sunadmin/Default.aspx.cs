﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class sunadmin_Default : System.Web.UI.Page
{
    string cookiesName = System.Configuration.ConfigurationManager.AppSettings["cookieName"];

    protected void Page_Load(object sender, EventArgs e)
    {
        #region NoBrowserCache
        HttpContext.Current.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
        HttpContext.Current.Response.Cache.SetValidUntilExpires(false);
        HttpContext.Current.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.Cache.SetNoStore();
        #endregion

        //Check cookies
        if (fnIsTokenValid(cookiesName))
        {
            //Exists and valid
            Response.Redirect("admin_.aspx");
        }
    }

    //Check whether cookies is created
    protected bool fnIsCookieCreated(string cookieName)
    {
        HttpCookie cookie = Request.Cookies[cookieName];
        if (cookie != null)
            return true;
        else
            return false;
    }

    //Check for valid authentication
    protected bool fnIsTokenValid(string cookieName)
    {
        if (fnIsCookieCreated(cookieName))
        {
            HttpCookie cookie = Request.Cookies[cookieName];

            using (dsAdminTableAdapters.taAdmin otaAdmin = new dsAdminTableAdapters.taAdmin())
            {
                dsAdmin.dtAdminDataTable odtAdmin = otaAdmin.fnGetUsernameByUsername(cookie["username"]);

                if (odtAdmin.Count > 0)
                {
                    if (cookie["token"] == string.Concat(odtAdmin[0].vchUsername, odtAdmin[0].vchPassword))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            return false;
        }
    }

    //Check user credentials
    protected bool fnCheckCreateCookie(string cookieName, string username, string password)
    {
        using (dsAdminTableAdapters.taAdmin otaAdmin = new dsAdminTableAdapters.taAdmin())
        {
            dsAdmin.dtAdminDataTable odtAdmin = otaAdmin.fnGetAdminByUsernamePassword(username, password);

            if (odtAdmin.Count > 0)
            {
                HttpCookie cookie = new HttpCookie(cookieName);
                TimeSpan lifeSpan = new TimeSpan(0, 2, 0, 0);
                DateTime dt = DateTime.Now;
                cookie.Expires = dt + lifeSpan;
                cookie["token"] = string.Concat(odtAdmin[0].vchUsername, odtAdmin[0].vchPassword);
                cookie["username"] = username;
                Response.Cookies.Add(cookie);
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request["g-recaptcha-response"]))
        {
            string sCatchaResponse = Request["g-recaptcha-response"];
            string sSecret = System.Configuration.ConfigurationManager.AppSettings["recaptcha:privateKey"];
            string sIPAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();

            System.Net.WebClient wc = new System.Net.WebClient();
            string sRequest = String.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}&remoteip={2}", sSecret, sCatchaResponse, sIPAddress);
            string sResponse = wc.DownloadString(sRequest);
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            GoogleResponse response = serializer.Deserialize<GoogleResponse>(sResponse);

            if (!response.success)
            {
                lblMsg.Text = "Please prove that you are not a robot";
                return;
            } 

            string vchUsername = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtUsername.Text);
            string vchPass = Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(txtPassword.Text);

            System.Security.Cryptography.SHA512 sha512 = new System.Security.Cryptography.SHA512Managed();

            byte[] password = sha512.ComputeHash(System.Text.Encoding.Default.GetBytes(vchUsername + vchPass));
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            foreach (Byte i in password)
            {
                sb.Append(i.ToString("X"));
            }

            if (fnCheckCreateCookie(cookiesName, vchUsername, sb.ToString()))
            {
                Response.Redirect("admin_.aspx");
            }
            else
            {
                lblMsg.Text = "Invalid Credential! Access Denied...";
            }

            //Create Admin
            //using (dsAdminTableAdapters.taAdmin otaAdmin = new dsAdminTableAdapters.taAdmin())
            //{
            //    if (otaAdmin.fnInsAdmin(txtUsername.Text, sb.ToString()) > 0)
            //    {
            //        lblMsg.Text = "Admin Access Added!";
            //    }
            //}
        }
        else
        {
            lblMsg.Text = "Please prove that you are not a robot";
            return;
        }
    }

    private class GoogleResponse
    {
        public bool success;
        public string errorcodes;
    }
}