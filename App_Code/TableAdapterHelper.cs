﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;

namespace TransactionHelper
{
    public class TableAdapterHelper
    {
        public TableAdapterHelper()
        {

        }

        public static SqlTransaction BeginTransaction(object tableAdapter)
        {
            return BeginTransaction(tableAdapter, IsolationLevel.ReadUncommitted);
        }

        public static SqlTransaction BeginTransaction(object tableAdapter, IsolationLevel isolationLevel)
        {
            // Get the table adapter's type
            Type type = tableAdapter.GetType();

            // Get the connection on the adapter
            SqlConnection connection = GetConnection(tableAdapter);

            // Make sure connection is open to start the transaction
            if (connection.State == ConnectionState.Closed)
                connection.Open();

            // Start a transaction on the connection
            SqlTransaction transaction = connection.BeginTransaction(isolationLevel);

            // Set the transaction on the table adapter
            SetTransaction(tableAdapter, transaction);

            return transaction;
        }

        // Gets the connection from the specified table adapter.
        private static SqlConnection GetConnection(object tableAdapter)
        {
            Type type = tableAdapter.GetType();
            PropertyInfo connectionProperty = type.GetProperty("Connection", BindingFlags.NonPublic | BindingFlags.Instance);
            SqlConnection connection = (SqlConnection)connectionProperty.GetValue(tableAdapter, null);
            return connection;
        }

        // Sets the connection on the specified table adapter.
        private static void SetConnection(object tableAdapter, SqlConnection connection)
        {
            Type type = tableAdapter.GetType();
            PropertyInfo connectionProperty = type.GetProperty("Connection", BindingFlags.NonPublic | BindingFlags.Instance);
            connectionProperty.SetValue(tableAdapter, connection, null);
        }

        // Enlists the table adapter in a transaction.
        public static void SetTransaction(object tableAdapter, SqlTransaction transaction)
        {
            // Get the table adapter's type
            Type type = tableAdapter.GetType();

            // Set the transaction on each command in the adapter
            PropertyInfo commandsProperty = type.GetProperty("CommandCollection", BindingFlags.NonPublic | BindingFlags.Instance);
            SqlCommand[] commands = (SqlCommand[])commandsProperty.GetValue(tableAdapter, null);
            foreach (SqlCommand command in commands)
                command.Transaction = transaction;

            // Set the connection on the table adapter
            SetConnection(tableAdapter, transaction.Connection);
        }
    }
}