﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="poll.aspx.cs" ValidateRequest="true" EnableViewStateMac="true" Inherits="poll" %>

<!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en" itemscope itemtype="http://schema.org/Product">
<!--<![endif]-->
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0" />
    <title>
        <asp:Literal ID="ltrTitle" runat="server"></asp:Literal>
    </title>
    <asp:Literal ID="ltrOgTitle" runat="server"></asp:Literal>
    <meta property="og:type" content="website" />
    <asp:Literal ID="ltrOgUrl" runat="server"></asp:Literal>
    <asp:Literal ID="ltrOgImg" runat="server"></asp:Literal>
    <asp:Literal ID="ltrOgDesc" runat="server"></asp:Literal>
    <meta name="robots" content="noindex" />
    <!-- For IE 9 and below. ICO should be 32x32 pixels in size -->
    <!--[if IE]><link rel="shortcut icon" href="favicon.ico"><![endif]-->

    <!-- Touch Icons - iOS and Android 2.1+ 180x180 pixels in size. -->
    <link rel="apple-touch-icon-precomposed" href="images/astro_.png">

    <!-- Firefox, Chrome, Safari, IE 11+ and Opera. 196x196 pixels in size. -->
    <link rel="icon" href="favicon.ico">
    <link href="css/normalize.css" rel="stylesheet" />
    <link href="css/poll.css" rel="stylesheet" />
    <asp:Literal ID="ltrJs" runat="server"></asp:Literal>
    <!--GA Tracking-->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-61848371-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>
    <div id="fb-root">
    </div>
    <form id="frmMain" runat="server" class="cd-form">
        <div id="parent" runat="server">
            <div id="topbanner">
                <asp:Literal ID="ltrBannerImg" runat="server"></asp:Literal>
            </div>
            <div id="midbannerparent">
                <div id="divFblogin" style="left: 50%; margin: -86px 0 0 -140px; position: absolute; top: 50%; display: none;">
                    <!-- FB Login-->
                    Get connected and start voting now.
                    <br />
                    <a id="btnFbLogin" class="btn-auth btn-facebook large">Facebook Connect</a>
                    <!-- End of FB Login-->
                </div>
                <div id="midbanner" style="display: none;">
                    <div style="padding-left: 10%;" id="shareWrapperDiv" runat="server">
                        <div class="fb-icon-bg"></div>
                        <div class="fb-bg" id="fbsharediv"></div>
                    </div>
                    <span id="title">
                        <asp:Literal ID="ltrVoteTitle" runat="server"></asp:Literal>
                    </span>
                    <hr style="width: 80%; align-self: center">
                    <asp:Literal ID="ltrViewResult" runat="server"></asp:Literal>
                    <p id="question">
                        <asp:Literal ID="ltrQuestion" runat="server"></asp:Literal>
                    </p>
                    <div id="form">
                        <asp:RadioButtonList ID="radbtnlstChoices" CssClass="cd-form-list" DataTextField="vchAnswertext" DataValueField="viId" runat="server" RepeatLayout="UnorderedList" RepeatDirection="Vertical"></asp:RadioButtonList>
                    </div>
                    <div id="submit">
                        <asp:Literal ID="ltrSubmitBtn" runat="server"></asp:Literal>
                    </div>
                    <div id="container" style="height: 400px; margin-left: auto; margin-right: auto; width: 80%; display: none"></div>
                    <hr id="submitline" style="width: 80%; align-self: center">
                    <p id="info">
                        <asp:Literal ID="ltrRemarks" runat="server"></asp:Literal>
                        <asp:HiddenField ID="hiddPollId" runat="server" />
                        <asp:HiddenField ID="hiddQuesId" runat="server" />
                    </p>
                    <!--facebook-like-->
                    <div style="font-size: medium; padding: 0% 0% 1% 10%;">
                        Don't forget to 'Like' our page and stay tune for upcoming updates!
                        <asp:Literal ID="ltrFBLike" runat="server"></asp:Literal>
                    </div>
                    <!-- end FB like -->
                </div>
            </div>
            <div id="footer">
                <p id="note">Copyright <%=DateTime.Now.Year %> Astro Interactive Services</p>
            </div>
        </div>
    </form>
    <div class="cd-popup" role="alert">
        <div class="cd-popup-container">
            <p id="pop-para">
                &nbsp;
            </p>
            <ul class="cd-buttons" id="cd-buttons-ul">
            </ul>
            <a class="cd-popup-close img-replace">Close</a>
        </div>
        <!-- cd-popup-container -->
    </div>
    <div id="loadingDiv" runat="server" style="display: none;">
        <img src="images/bold.gif" alt="Loading" />
    </div>
    <!-- JavaScript at the bottom for fast page loading -->
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//connect.facebook.net/en_US/all.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script src="js/highcharts.js" type="text/javascript" charset="UTF-8"></script>
    <script src="js/highcharts-3d.js" type="text/javascript" charset="UTF-8"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            FB.init({
                appId: '1596929773927575',
                cookie: true,
                oauth: true,
                status: true,      // Check Facebook Login status
                xfbml: true,
                version: 'v2.1'
            });

            fnCheckLoginStatus();

            $('#btnFbLogin').on('click', function (event) {
                ga('send', 'event', 'button', 'click', 'Fb-Login-btn');
                fnFbLogin();
            });

            $('#btnVote').on('click', function (event) {
                if ((typeof $("#radbtnlstChoices input:checked").val() != "undefined")) {
                    ga('send', 'event', 'button', 'click', 'Vote-btn');

                    var vpid = encodeURIComponent($('#hiddPollId').val());
                    var vqid = encodeURIComponent($('#hiddQuesId').val());
                    var vaid = encodeURIComponent($("#radbtnlstChoices input:checked").val());

                    var jsonData = {
                        'fbid': $.cookie("fbid"),
                        'fblink': $.cookie("fblink"),
                        'fbname': $.cookie("fbname"),
                        'email': $.cookie("fbemail"),
                        'ip': '',
                        'pid': vpid,
                        'qid': vqid,
                        'aid': vaid
                    };

                    $.ajax({
                        url: "vote.ashx/ProcessRequest",
                        cache: false,
                        type: 'POST',
                        data: jsonData,
                        async: false,
                        beforeSend: function () {
                            var docHeight = $(document).height();
                            $("#loadingDiv").height(docHeight)
                            .css({
                                'opacity': 0.85,
                                'position': 'absolute',
                                'top': '0px',
                                'left': '50px',
                                'margin-left': '-50px',
                                'background-color': 'white',
                                'width': '100%',
                                'z-index': 5000,
                            });
                            $('body').addClass('stop-scrolling');
                            $('#loadingDiv').show();
                        },
                        success: function (response) {
                            if (response.result_code === 1) {
                                $('#cd-buttons-ul').html("<li style='width: 50%;'><a style='cursor: pointer' class='cd-buttons-result'>View Result</a></li><li style='width: 50%;'><a style='cursor: pointer' class='cd-buttons-close'>Close</a></li>");
                                //$('#btnViewResult').css("display", "inherit");
                                //$('#afterbtnline').show();
                            }
                            else {
                                $('#cd-buttons-ul').html("<li style='width: 100%;'><a style='cursor: pointer' class='cd-buttons-close'>Close</a></li>");
                            }
                            $('#pop-para').text(response.msg);
                            $('#pop-para').css("color", '#' + response.popTxtColor);
                            $('.cd-popup-container').css("background", '#' + response.popBgColor);
                            $('.cd-popup').addClass('is-visible');
                            $('#radbtnlstChoices input').attr('checked', false);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $('#cd-buttons-ul').html("<li style='width: 100%;'><a style='cursor: pointer' class='cd-buttons-close'>Close</a></li>");
                            $('#pop-para').text(xhr.responseText);
                            $('#pop-para').css("color", '#000');
                            $('.cd-popup-container').css("background", '#fff');
                            $('.cd-popup').addClass('is-visible');
                            console.log(xhr.responseText);
                        },
                        complete: function () {
                            $('body').removeClass('stop-scrolling');
                            $('#loadingDiv').hide();
                        }
                    });
                }
            });

            //close popup
            $('.cd-popup').on('click', function (event) {
                if ($(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup') || $(event.target).is('.cd-buttons-close')) {
                    event.preventDefault();
                    $(this).removeClass('is-visible');
                }
            });

            //result btn popup
            $('.cd-popup').on('click', function (event) {
                if ($(event.target).is('.cd-buttons-result')) {
                    event.preventDefault();
                    $("#btnViewResult").trigger("click");
                    $(this).removeClass('is-visible');
                }
            });

            $('#btnViewResult').on('click', function (event) {
                event.preventDefault();
                ga('send', 'event', 'button', 'click', 'View-Result-btn');
                var vpid = encodeURIComponent($('#hiddPollId').val());
                var vqid = encodeURIComponent($('#hiddQuesId').val());
                var jsonQuery = {
                    'pid': vpid,
                    'qid': vqid
                };

                var chartdata = [];
                $.ajax({
                    url: "chartresult.ashx/ProcessRequest",
                    cache: false,
                    type: 'GET',
                    data: jsonQuery,
                    async: false,
                    beforeSend: function () {
                        $('#form').hide();
                        $('#submit').hide();
                        $('#container').show();
                    },
                    success: function (response) {
                        if (response.chart_type === 'pie') {
                            var arr = response.options;
                            for (var i = 0; i < arr.length; i++) {
                                temparray = [arr[i].option_text, arr[i].total_percentage];
                                chartdata.push(temparray);
                            }
                            $('#container').show();
                            var chart = new Highcharts.Chart({
                                chart: {
                                    renderTo: 'container',
                                    type: 'pie',
                                    options3d: {
                                        enabled: true,
                                        alpha: 45,
                                        beta: 0
                                    }
                                },
                                title: {
                                    text: null
                                },
                                tooltip: {
                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                },
                                plotOptions: {
                                    pie: {
                                        allowPointSelect: true,
                                        cursor: 'pointer',
                                        depth: 35,
                                        dataLabels: {
                                            enabled: true,
                                            format: '{point.name}: {point.percentage:.1f}%',
                                            distance: 20
                                        }
                                    }
                                },
                                credits: {
                                    enabled: false
                                },
                                series: [{
                                    type: 'pie',
                                    name: 'Percentage',
                                    data: chartdata
                                }]
                            });
                        }
                        else if (response.chart_type === 'bar') {
                            var arr = response.options;
                            var chartXaxis = [];
                            for (var i = 0; i < arr.length; i++) {
                                temparray = [arr[i].option_text, arr[i].total_percentage];
                                chartdata.push(temparray);
                                temparray = [arr[i].option_text];
                                chartXaxis.push(temparray);
                            }
                            $('#container').show();
                            var chart = new Highcharts.Chart({
                                chart: {
                                    renderTo: 'container',
                                    type: 'bar'
                                },
                                title: {
                                    text: null
                                },
                                xAxis: {
                                    categories: chartXaxis,
                                    title: {
                                        text: null
                                    }
                                },
                                yAxis: {
                                    min: 0,
                                    max: 100,
                                    title: {
                                        text: null,
                                        align: 'high'
                                    },
                                    labels: {
                                        overflow: 'justify'
                                    }
                                },
                                tooltip: {
                                    valueSuffix: ' %'
                                },
                                plotOptions: {
                                    bar: {
                                        dataLabels: {
                                            enabled: true,
                                            format: '{point.y:.2f}%'
                                        }
                                    }
                                },
                                credits: {
                                    enabled: false
                                },
                                series: [
                                    {
                                        name: 'Percentage',
                                        data: chartdata
                                    }
                                ]
                            });
                        }
                        else {
                            $('#cd-buttons-ul').html("<li style='width: 100%;'><a style='cursor: pointer' class='cd-buttons-close'>Close</a></li>");
                            $('#pop-para').text(response.title);
                            $('#pop-para').css("color", '#000');
                            $('.cd-popup-container').css("background", '#fff');
                            $('.cd-popup').addClass('is-visible');
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $('#cd-buttons-ul').html("<li style='width: 100%;'><a style='cursor: pointer' class='cd-buttons-close'>Close</a></li>");
                        $('#pop-para').text(xhr.responseText);
                        $('#pop-para').css("color", '#000');
                        $('.cd-popup-container').css("background", '#fff');
                        $('.cd-popup').addClass('is-visible');
                        console.log(xhr.responseText);
                    },
                    complete: function () {
                        $('#btnBacktovote').css("display", "inherit");
                        $('#btnViewResult').hide();
                    }
                });
            });

            $('#btnBacktovote').on('click', function (event) {
                event.preventDefault();
                $('#btnViewResult').css("display", "inherit");
                $('#afterbtnline').show();
                $('#btnBacktovote').hide();
                $('#container').empty().hide();
                //$('#container').highcharts().destroy();                
                $('#form').show();
                $('#submit').show();
            });

            $('#fbsharediv').on('click', function (event) {
                event.preventDefault();
                ga('send', 'event', 'button', 'click', 'Share-btn');
                FB.ui({
                    method: 'share',
                    href: $(location).attr('href')
                }, function (response) {
                    console.dir(response);
                    if (response.error_code != 4201) {
                        $('#cd-buttons-ul').html("<li style='width: 100%;'><a style='cursor: pointer' class='cd-buttons-close'>Close</a></li>");
                        $('#pop-para').text("Success: Posted on Facebook.");
                        $('#pop-para').css("color", '#000');
                        $('.cd-popup-container').css("background", '#fff');
                        $('.cd-popup').addClass('is-visible');
                    }
                });
            });

            FB.Canvas.setAutoGrow(100);
        });
    </script>
    <script src="js/fb.js" type="text/javascript"></script>
</body>
</html>
