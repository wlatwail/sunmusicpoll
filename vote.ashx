﻿<%@ WebHandler Language="C#" Class="vote" %>

using System;
using System.Web;
using Microsoft.Security.Application;

public class vote : IHttpHandler
{

    public void ProcessRequest(HttpContext context) {
        
        //Status Code: 0 - x vote; 1 - Vote; 2 - Voted
        //Result code: 0 - x show; 1 - Show

        System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
        string vchFbid = Sanitizer.GetSafeHtmlFragment(context.Request["fbid"]);
        string vchFblink = Sanitizer.GetSafeHtmlFragment(context.Request["fblink"]);
        string vchFbname = Sanitizer.GetSafeHtmlFragment(context.Request["fbname"]);
        string vchIpAddress = string.IsNullOrEmpty(context.Request["ip"]) ? context.Request.ServerVariables["REMOTE_ADDR"] : ".";
        string vchPollId = Sanitizer.GetSafeHtmlFragment(context.Request["pid"]);
        string vchQuestionId = Sanitizer.GetSafeHtmlFragment(context.Request["qid"]);
        string vchAnswerId = Sanitizer.GetSafeHtmlFragment(context.Request["aid"]);
        string vchEmail = string.IsNullOrEmpty(context.Request["fbemail"]) ? string.Empty : Sanitizer.GetSafeHtmlFragment(context.Request["fbemail"]);

        //Casting
        int viPollId = 0;
        int.TryParse(vchPollId, out viPollId);

        int viAnswerId = 0;
        int.TryParse(vchAnswerId, out viAnswerId);

        Response rsp;

        if (string.IsNullOrEmpty(vchFbid) || string.IsNullOrEmpty(vchFblink) || string.IsNullOrEmpty(vchFbname) || string.IsNullOrEmpty(vchIpAddress) || string.IsNullOrEmpty(vchPollId) || string.IsNullOrEmpty(vchQuestionId) || string.IsNullOrEmpty(vchAnswerId))
        {
            rsp = new Response(0, 0, "[Operation aborted]: Invalid vote request.", "000", "FFF");
        }
        else
        {
            using (dsPollTableAdapters.taPoll otaPoll = new dsPollTableAdapters.taPoll())
            {
                dsPoll.dtPollDataTable odtPoll = otaPoll.fnPollById(viPollId);
                if (odtPoll.Count > 0)
                {
                    using (dsQuestionTableAdapters.taQuestion otaQuestion = new dsQuestionTableAdapters.taQuestion())
                    {
                        dsQuestion.dtQuestionDataTable odtQuestion = otaQuestion.fnQuestionById(vchQuestionId, viPollId);
                        if (odtQuestion.Count > 0)
                        {
                            int viShowResult = 0;
                            if ((odtQuestion[0].viShowResult == 1) || (odtQuestion[0].viShowResult == 2 && odtQuestion[0].vdtShowResultDT <= DateTime.Today))
                                viShowResult = 1;

                            //Checking
                            if (!odtPoll[0].vblShowPoll)
                            {
                                rsp = new Response(0, 0, "[Operation aborted]: Poll is available for voting.", odtQuestion[0].vchPopBgColor, odtQuestion[0].vchPopFontColor);
                            }
                            else if (DateTime.Today >= odtQuestion[0].vdtPollStart && DateTime.Today <= odtQuestion[0].vdtPollEnd)
                            {
                                if (odtQuestion[0].vblIsVoteCapped)
                                {
                                    using (dsVotesTableAdapters.taVotes otaVotes = new dsVotesTableAdapters.taVotes())
                                    {
                                        dsVotes.dtVotesDataTable odtVotes = otaVotes.fnTodayVotes(vchFbid, viPollId, vchQuestionId, DateTime.Today);
                                        if (odtVotes.Count >= odtQuestion[0].viVoteLimit)
                                            rsp = new Response(2, viShowResult, "[Unsuccessful]: Vote limit reached, only " + odtQuestion[0].viVoteLimit + " vote(s) allowed per day.", odtQuestion[0].vchPopBgColor, odtQuestion[0].vchPopFontColor);
                                        else if (odtVotes.Count < odtQuestion[0].viVoteLimit)
                                        {
                                            if (otaVotes.fnInsVotes(viAnswerId, vchFbid, DateTime.Now, viPollId, vchIpAddress, vchQuestionId) > 0)
                                            {
                                                rsp = new Response(1, viShowResult, odtQuestion[0].vchPopMsg, odtQuestion[0].vchPopBgColor, odtQuestion[0].vchPopFontColor);
                                            }
                                            else
                                            {
                                                rsp = new Response(1, viShowResult, "[Error]: Failed to perform vote action.", odtQuestion[0].vchPopBgColor, odtQuestion[0].vchPopFontColor);
                                            }
                                        }
                                        else
                                        {
                                            rsp = new Response(2, viShowResult, "[Error]: Vote limit exceeded. ", odtQuestion[0].vchPopBgColor, odtQuestion[0].vchPopFontColor);
                                        }
                                    }
                                }
                                else //No vote capping, INSERT
                                {
                                    using (dsVotesTableAdapters.taVotes otaVotes = new dsVotesTableAdapters.taVotes())
                                    {
                                        if (otaVotes.fnInsVotes(viAnswerId, vchFbid, DateTime.Now, viPollId, vchIpAddress, vchQuestionId) > 0)
                                        {
                                            rsp = new Response(1, viShowResult, odtQuestion[0].vchPopMsg, odtQuestion[0].vchPopBgColor, odtQuestion[0].vchPopFontColor);
                                        }
                                        else
                                        {
                                            rsp = new Response(1, 0, "[Error]: Failed to vote.", odtQuestion[0].vchPopBgColor, odtQuestion[0].vchPopFontColor);
                                        }
                                    }
                                }
                                
                                //[User] Table
                                using (dsUserTableAdapters.taUser otaUser = new dsUserTableAdapters.taUser())
                                {
                                    dsUser.dtUserDataTable odtUser = otaUser.fnUserByFbid(vchFbid);
                                    if (odtUser.Count > 0)
                                    {
                                        //User exists in [User] table
                                    }
                                    else //INSERT
                                    {
                                        if (otaUser.fnInsUser(vchFbid, vchFblink, vchFbname, "https://graph.facebook.com/" + vchFbid + "/picture?type=large", DateTime.Now, vchEmail) > 0)
                                        {
                                            //Successful
                                        }
                                    }
                                }
                            }
                            else
                            {
                                rsp = new Response(0, 0, "[Operation denied]: Attempt to vote outside of voting period.", odtQuestion[0].vchPopBgColor, odtQuestion[0].vchPopFontColor);
                            }
                        }
                        else
                        {
                            rsp = new Response(0, 0, "[Operation aborted]: Intend to vote for an invalid question.", odtQuestion[0].vchPopBgColor, odtQuestion[0].vchPopFontColor);
                        }
                    }
                }
                else
                {
                    rsp = new Response(0, 0, "[Operation aborted]: Poll is invalid.", "000", "FFF");
                }
            }
        }

        string json = js.Serialize(rsp);
        context.Response.Clear();
        context.Response.ContentType = "application/json; charset=utf-8";
        context.Response.Write(json);
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
        
    public class Response
    {
        public int status_code { get; set; }
        public int result_code { get; set; }
        public string msg { get; set; }
        public string popBgColor { set; get; }
        public string popTxtColor { set; get; }

        public Response(int status_code, int result_code, string msg, string popBgColor, string popTxtColor)
        {
            this.status_code = status_code;
            this.result_code = result_code;
            this.msg = msg;
            this.popBgColor = popBgColor;
            this.popTxtColor = popTxtColor;
        }
    }

}