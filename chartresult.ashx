﻿<%@ WebHandler Language="C#" Class="chartresult" %>

using System;
using System.Web;
using Microsoft.Security.Application;

public class chartresult : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
        string vchPollId = Sanitizer.GetSafeHtmlFragment(context.Request["pid"]);
        string vchQuestionId = Sanitizer.GetSafeHtmlFragment(context.Request["qid"]);

        //Casting
        int viPollId = 0;
        int.TryParse(vchPollId, out viPollId);

        Response rsp;

        if (string.IsNullOrEmpty(vchPollId) || string.IsNullOrEmpty(vchQuestionId))
        {
            rsp = new Response("none", string.Empty);
        }
        else
        {
            using (dsPollTableAdapters.taPoll otaPoll = new dsPollTableAdapters.taPoll())
            {
                dsPoll.dtPollDataTable odtPoll = otaPoll.fnPollById(viPollId);
                if (odtPoll.Count > 0)
                {
                    using (dsQuestionTableAdapters.taQuestion otaQuestion = new dsQuestionTableAdapters.taQuestion())
                    {
                        dsQuestion.dtQuestionDataTable odtQuestion = otaQuestion.fnQuestionById(vchQuestionId, viPollId);
                        if (odtQuestion.Count > 0)
                        {
                            using (dsAnswerTableAdapters.taAnswer otaAnswer = new dsAnswerTableAdapters.taAnswer())
                            {
                                dsAnswer.dtAnswerDataTable odtAnswer = otaAnswer.fnQuestionByPollIdQuestionId(viPollId, vchQuestionId);
                                if (odtAnswer.Count > 0)
                                {
                                    using (dsVotesTableAdapters.taVotesAllTotalVotes otaVotesAllTotalVotes = new dsVotesTableAdapters.taVotesAllTotalVotes())
                                    {
                                        dsVotes.dtVotesAllTotalVotesDataTable odtVotesAllTotalVotes = otaVotesAllTotalVotes.fnAllTotalVotes(viPollId,vchQuestionId, odtQuestion[0].vdtPollStart,odtQuestion[0].vdtPollEnd);
                                        Option[] opt = new Option[odtAnswer.Count];
                                        if (odtVotesAllTotalVotes.Count > 0)
                                        {
                                            double vdblTotal = 0.00f;
                                            for (int i = 0; i < odtVotesAllTotalVotes.Count; i++)
                                            {
                                                vdblTotal += odtVotesAllTotalVotes[i].viTotal;
                                            }

                                            for (int i = 0; i < odtAnswer.Count; i++)
                                            {
                                                int z = -1;
                                                for (int j = 0; j < odtVotesAllTotalVotes.Count; j++)
                                                {
                                                    if (odtVotesAllTotalVotes[j].viAnswerIdFk == odtAnswer[i].viId)
                                                    {
                                                        z = j;
                                                        break;
                                                    }
                                                }
                                                
                                                if (z == -1)
                                                {
                                                    opt[i] = new Option(odtAnswer[i].viId, odtAnswer[i].vchAnswertext, 0, 0.00f);
                                                }
                                                else
                                                {
                                                    opt[i] = new Option(odtAnswer[i].viId, odtAnswer[i].vchAnswertext, odtVotesAllTotalVotes[z].viTotal, Math.Round(((odtVotesAllTotalVotes[z].viTotal / vdblTotal) * 100.00f), 2));
                                                }                                                
                                            }
                                        }
                                        else // all 0
                                        {
                                            for (int i = 0; i < odtAnswer.Count; i++)
                                            {
                                                opt[i] = new Option(odtAnswer[i].viId, odtAnswer[i].vchAnswertext, 0, 0.00f);
                                            }
                                        }

                                        rsp = new Response(odtQuestion[0].viResultType == 1 ? "pie" : "bar", HttpUtility.HtmlDecode(odtQuestion[0].vchQuestiontext), opt);
                                    }
                                }
                                else
                                {
                                    rsp = new Response("none", "[Invalid]: Options does not exist.");
                                }
                            }
                        }
                        else
                        {
                            rsp = new Response("none", "[Invalid]: Poll and Question does not exist.");
                        }
                    }
                }
                else
                {
                    rsp = new Response("none", "[Invalid]: Poll does not exist.");
                }
            }
        }

        string json = js.Serialize(rsp);
        context.Response.Clear();
        context.Response.ContentType = "application/json; charset=utf-8";
        context.Response.Write(json);
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }
    
    public class Response
    {
        public string chart_type { get; set; }
        public string title { get; set; }
        public Option[] options { get; set; }
        
        public Response(string chart_type, string title)
        {
            this.chart_type = chart_type;
            this.title = title;
        }
        
        public Response(string chart_type, string title, Option[] options)
        {
            this.chart_type = chart_type;
            this.title = title;
            this.options = options;
        }         
    }

    public class Option
    {
        public int id { get; set; }
        public string option_text { get; set; }
        public int total { get; set; }
        public double total_percentage { get; set; }
        
        public Option(int id, string option_text, int total, double total_percentage)
        {
            this.id = id;
            this.option_text = option_text;
            this.total = total;
            this.total_percentage = total_percentage;
        }
    }
}