﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Security.Application; 

public partial class poll : System.Web.UI.Page
{
    string cookiesName = System.Configuration.ConfigurationManager.AppSettings["cookieName"];

    protected void Page_Load(object sender, EventArgs e)
    {
        #region NoBrowserCache
        HttpContext.Current.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
        HttpContext.Current.Response.Cache.SetValidUntilExpires(false);
        HttpContext.Current.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.Cache.SetNoStore();
        #endregion

        if (!IsPostBack) //First Loads
        {
            if (!string.IsNullOrEmpty(Request.QueryString["pid"]))
            {
                string vchQSPollId = Sanitizer.GetSafeHtmlFragment(Request.QueryString["pid"]); 
                int viPollId = 0;
                if(int.TryParse(vchQSPollId, out viPollId))
                {
                    using (dsPollTableAdapters.taPoll otaPoll = new dsPollTableAdapters.taPoll())
                    {
                        dsPoll.dtPollDataTable odtPoll = otaPoll.fnPollById(viPollId);
                        if (odtPoll.Count > 0)
                        {
                            //Check cookies
                            if (!fnIsTokenValid(cookiesName)) //Not admin
                            {
                                HttpRequest httpRequest = HttpContext.Current.Request;
                                if (!httpRequest.Browser.IsMobileDevice)
                                {
                                    if (!string.IsNullOrEmpty(odtPoll[0].vchFbAppId))
                                        ltrJs.Text = "<script>if (top == self) { top.location = 'https://www.facebook.com/pages/-/" + odtPoll[0].vchFbPageId + "?sk=app_" + odtPoll[0].vchFbAppId + "\'; }</script>";
                                }
                            }

                            ltrTitle.Text = odtPoll[0].vchName + " Poll";
                            ltrBannerImg.Text = "<img alt=\"" + odtPoll[0].vchBannerFilename + "\" src=\"upload/banner/" + odtPoll[0].vchBannerFilename + "\" />";
                            ltrRemarks.Text = odtPoll[0].vchRemarks;
                            if (!string.IsNullOrEmpty(odtPoll[0].vchFbPageId))
                            {
                                ltrFBLike.Text = "<div class=\"fb-like\" data-href=\"https://www.facebook.com/pages/-/" + odtPoll[0].vchFbPageId + "\" data-width=\"450\" data-show-faces=\"true\" data-share=\"false\" data-layout=\"standard\"></div>";
                            }

                            using (dsQuestionTableAdapters.taQuestion otaQuestion = new dsQuestionTableAdapters.taQuestion())
                            {
                                dsQuestion.dtQuestionDataTable odtQuestion = otaQuestion.fnTodayQuestion(viPollId, DateTime.Today);
                                if (odtQuestion.Count > 0)
                                {
                                    ltrOgTitle.Text = "<meta property=\"og:title\" content=\"" + odtQuestion[0].vchQuestiontext + " | " + odtPoll[0].vchName + " Poll\" />";
                                    // ltrOgUrl.Text = "<meta property=\"og:url\" content=\"https://" + Request.Url.Host + Request.CurrentExecutionFilePath + "?pid=2\" />";
                                    ltrOgImg.Text = "<meta property=\"og:image\" content=\"https://" + Request.Url.Host + Request.CurrentExecutionFilePath.Replace("poll.aspx", "") + "upload/banner/" + odtPoll[0].vchBannerFilename + "\" />";
                                    ltrOgDesc.Text = string.IsNullOrEmpty(odtQuestion[0].vchFbCtaOgDescription) ? "<meta property=\"og:description\" content=\"" + odtPoll[0].vchRemarks + "\" />" : "<meta property=\"og:description\" content=\"" + odtQuestion[0].vchFbCtaOgDescription + "\" />";
                                    parent.Style.Add("background-color", "#" + odtQuestion[0].vchPollBgColor);
                                    parent.Style.Add("color", "#" + odtQuestion[0].vchPollFontColor);

                                    if (odtPoll[0].vblShowPoll)
                                    {
                                        ltrVoteTitle.Text = odtQuestion[0].vchTitle;
                                        ltrQuestion.Text = odtQuestion[0].vchQuestiontext;
                                        if (odtQuestion[0].viShowResult == 1 || (odtQuestion[0].viShowResult == 2 && odtQuestion[0].vdtShowResultDT <= DateTime.Today))
                                            ltrViewResult.Text = "<a id=\"btnViewResult\" class=\"progress-button\"  style=\"display: inherit; text-align: center; width: 160px; border: 2px solid #" + odtQuestion[0].vchPollFontColor + "; color: #" + odtQuestion[0].vchPollFontColor + ";\">View Result</a><a id=\"btnBacktovote\" class=\"progress-button\" style=\"display: none; text-align: center; width: 160px; border: 2px solid #" + odtQuestion[0].vchPollFontColor + "; color: #" + odtQuestion[0].vchPollFontColor + ";\"><< Back</a><hr id=\"afterbtnline\" style=\"width: 80%; align-self: center;\">";
                                        else
                                            ltrViewResult.Text = "<a id=\"btnViewResult\" class=\"progress-button\"  style=\"display: none; text-align: center; width: 160px; border: 2px solid #" + odtQuestion[0].vchPollFontColor + "; color: #" + odtQuestion[0].vchPollFontColor + ";\">View Result</a><a id=\"btnBacktovote\" class=\"progress-button\" style=\"display: none; text-align: center; width: 160px; border: 2px solid #" + odtQuestion[0].vchPollFontColor + "; color: #" + odtQuestion[0].vchPollFontColor + ";\"><< Back</a><hr id=\"afterbtnline\" style=\"width: 80%; align-self: center; display: none;\">";
                                        ltrSubmitBtn.Text = "<div id=\"divSubBtn\"><a id=\"btnVote\" style=\"cursor:pointer;\"><img alt=\"" + odtPoll[0].vchBtnFilename + "\" src=\"upload/btn/" + odtPoll[0].vchBtnFilename + "\" /></a></div>";
                                        using (dsAnswerTableAdapters.taAnswer otaAnswer = new dsAnswerTableAdapters.taAnswer())
                                        {
                                            radbtnlstChoices.DataSource = otaAnswer.fnQuestionByPollIdQuestionId(viPollId, odtQuestion[0].vchQuestionId);
                                            radbtnlstChoices.DataBind();
                                        }
                                    }
                                    else
                                    {
                                        shareWrapperDiv.Style.Add("display", "none");
                                        ltrQuestion.Text = "No poll is available at the moment. Please come back again.";
                                    }

                                    hiddPollId.Value = odtPoll[0].viId.ToString();
                                    hiddQuesId.Value = odtQuestion[0].vchQuestionId;
                                }
                                else //Poll is not available for voting
                                {
                                    using (dsVotesTableAdapters.taVotes otaVotes = new dsVotesTableAdapters.taVotes())
                                    {
                                        dsVotes.dtVotesDataTable odtVotes = otaVotes.fnTodayLatestVotesByPollId(viPollId, DateTime.Today);
                                        if (odtVotes.Count > 0)
                                        {
                                            odtQuestion = otaQuestion.fnQuestionById(odtVotes[0].vchQuestion_id_fk, viPollId);
                                            if (odtQuestion.Count > 0)
                                            {
                                                ltrQuestion.Text = odtQuestion[0].vchQuestiontext;
                                                if (odtQuestion[0].viShowResult == 1 || (odtQuestion[0].viShowResult == 2 && odtQuestion[0].vdtShowResultDT <= DateTime.Today))
                                                {
                                                    ltrViewResult.Text = "<a id=\"btnViewResult\"  class=\"progress-button\" style=\"display: inherit; text-align: center; width: 160px; border: 2px solid #" + odtQuestion[0].vchPollFontColor + "; color: #" + odtQuestion[0].vchPollFontColor + ";\">View Result</a><a id=\"btnBacktovote\" class=\"progress-button\" style=\"display: none; text-align: center; width: 160px; border: 2px solid #" + odtQuestion[0].vchPollFontColor + "; color: #" + odtQuestion[0].vchPollFontColor + ";\"><< Back</a><hr id=\"afterbtnline\" style=\"width: 80%; align-self: center;\">";
                                                    ltrVoteTitle.Text = "The voting is now closed. <br/>Click 'View Result' to check the vote's standing. ";
                                                }
                                                else
                                                {
                                                    ltrViewResult.Text = "<a id=\"btnViewResult\"  class=\"progress-button\" style=\"display: none; text-align: center; width: 160px; border: 2px solid #" + odtQuestion[0].vchPollFontColor + "; color: #" + odtQuestion[0].vchPollFontColor + ";\">View Result</a><a id=\"btnBacktovote\" class=\"progress-button\" style=\"display: none; text-align: center; width: 160px; border: 2px solid #" + odtQuestion[0].vchPollFontColor + "; color: #" + odtQuestion[0].vchPollFontColor + ";\"><< Back</a><hr id=\"afterbtnline\" style=\"width: 80%; align-self: center; display: none;\">";
                                                    ltrVoteTitle.Text = "The voting is now closed. <br/>Stay tuned for the result announcement. ";
                                                }
                                                using (dsAnswerTableAdapters.taAnswer otaAnswer = new dsAnswerTableAdapters.taAnswer())
                                                {
                                                    radbtnlstChoices.DataSource = otaAnswer.fnQuestionByPollIdQuestionId(viPollId, odtQuestion[0].vchQuestionId);
                                                    radbtnlstChoices.DataBind();
                                                }
                                                hiddPollId.Value = odtPoll[0].viId.ToString();
                                                hiddQuesId.Value = odtQuestion[0].vchQuestionId;
                                            }
                                            else
                                            {
                                                ltrQuestion.Text = "No poll is available at the moment. Please come back again.";
                                            }
                                            shareWrapperDiv.Style.Add("display", "none");
                                        }
                                        else
                                        {
                                            shareWrapperDiv.Style.Add("display", "none");
                                            ltrQuestion.Text = "No poll is available at the moment. Please come back again.";
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            Response.Redirect("https://aiservices.astro.com.my/fbpoll/");
                            //ClientScript.RegisterStartupScript(GetType(), "Load", "<script type='text/javascript'>window.parent.location.href = 'https://aiservices.astro.com.my/fbpoll/'; </script>");
                        }
                    }
                }
                else
                {
                    Response.Redirect("https://aiservices.astro.com.my/fbpoll/");
                }                
            }
            else
            {
                Response.Redirect("https://aiservices.astro.com.my/fbpoll/");
                //ClientScript.RegisterStartupScript(GetType(), "Load", "<script type='text/javascript'>window.parent.location.href = 'https://aiservices.astro.com.my/fbpoll/'; </script>");
            }
        }
    }

    //Check whether cookies is created
    protected bool fnIsCookieCreated(string cookiesName)
    {
        HttpCookie cookie = Request.Cookies[cookiesName];
        if (cookie != null)
            return true;
        else
            return false;
    }

    //Check for valid authentication
    protected bool fnIsTokenValid(string cookieName)
    {
        if (fnIsCookieCreated(cookieName))
        {
            HttpCookie cookie = Request.Cookies[cookieName];
            using (dsAdminTableAdapters.taAdmin otaAdmin = new dsAdminTableAdapters.taAdmin())
            {
                dsAdmin.dtAdminDataTable odtAdmin = otaAdmin.fnGetUsernameByUsername(cookie["username"]);
                if (odtAdmin.Count > 0)
                {
                    if (cookie["token"] == string.Concat(odtAdmin[0].vchUsername, odtAdmin[0].vchPassword))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            return false;
        }
    }
}